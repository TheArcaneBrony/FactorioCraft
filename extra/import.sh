#!/bin/sh
#echo 'Downloading data for factorio 1.1.53...'
#wget https://gist.githubusercontent.com/Bilka2/6b8a6a9e4a4ec779573ad703d03c1ae7/raw
#echo 'Removing lua comments and other noise...'
#mv -v raw factorio.json
#sed 's/--.*$//g' factorio.json > stripped.json
#sed -i 's/Script @__DataRawSerpent__\/data-final-fixes.lua:1: //g' stripped.json
#echo 'Fixing json file...'
#sed -i -E 's/layers = \{(.*)\}/layers = \[\1\]/g' stripped.json
#sed -i -E 's/shift = \{(.*)\}/shift = \[\1\]/g' stripped.json --debug
#sed -i -E 's/shift\s=\s{/shift\s=\s[/g/s/}, /],/g' stripped.json
#sed -ir "s/shift\s\=\s\[{]/shift\s\=\s\[[]/g/s/},\s\/{]},/g" stripped.json
#sed -ir "s/shift\s\=\s\[{]/shift\s\=\s\{[}/g/s/[}],\s\/{]},/g" stripped.json
cp -v ~/.local/share/Steam/steamapps/common/Factorio/data/base/data.lua .
cp -v json.lua ~/.local/share/Steam/steamapps/common/Factorio/data/base/
echo 'local json = require("json")' >> ~/.local/share/Steam/steamapps/common/Factorio/data/base/data.lua
echo 'print(json.stringify(data))' >> ~/.local/share/Steam/steamapps/common/Factorio/data/base/data.lua
~/.local/share/Steam/steamapps/common/Factorio/bin/x64/factorio | grep '{"raw":{"font"' | jq > dump.json
mv -v data.lua ~/.local/share/Steam/steamapps/common/Factorio/data/base/
