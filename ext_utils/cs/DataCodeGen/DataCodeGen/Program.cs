﻿// See https://aka.ms/new-console-template for more information

using DataCodeGen;

Console.WriteLine("Hello, World!");

if (Directory.Exists("out"))
{
    Directory.Delete("out", true);
}

Directory.CreateDirectory("out");
LanguageGenerator.Run();
ResearchGenerator.Run();