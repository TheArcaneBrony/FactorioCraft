using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace DataCodeGen;

public class LanguageGenerator
{
    public static void Run()
    {
        Directory.CreateDirectory("out/lang");
        string cat = "";
        if (Directory.Exists(DataSources.ProjectAssetsDir + $"/lang"))
            Directory.Delete(DataSources.ProjectAssetsDir + $"/lang", true);
        Directory.CreateDirectory(DataSources.ProjectAssetsDir + $"/lang");
        foreach (var lang in Directory.GetDirectories(DataSources.GameBaseDir + "/data/base/locale/"))
        {
            if (!File.Exists(DataSources.ProjectAssetsDir + $"/lang_base/{toLang(new FileInfo(lang).Name)}.json"))
            {
                //Console.WriteLine(
                //    $"Skipping {toLang(new FileInfo(lang).Name)} as lang_base/{toLang(new FileInfo(lang).Name)}.json does not exist");
                continue;
            }

            var langJson = JsonSerializer
                .Deserialize<JsonObject>(File.ReadAllText(DataSources.ProjectAssetsDir +
                                                          $"/lang_base/{toLang(new FileInfo(lang).Name)}.json"))
                .AsObject();
            //Console.WriteLine(langJson.ToJsonString());
            var lines = File.ReadAllLines(lang + "/base.cfg");
            int i = 0;

            foreach (var line in lines)
            {
                Console.Write(
                    $"\rGenerating language file for {new FileInfo(lang).Name} -> {toLang(new FileInfo(lang).Name)}: ({++i})/({lines.Length})!");
                if (line.StartsWith("["))
                {
                    cat = line.Replace("[", "").Replace("]", "");
                    continue;
                }

                if (cat == "map-gen-preset-name") continue;
                if (cat == "map-gen-preset-description") continue;
                if (cat == "tips-and-tricks-item-name") continue;
                if (cat == "tips-and-tricks-item-description") continue;

                if (line.Length < 2) continue;
                var key = getKey(cat, line.Split("=")[0]);
                if (langJson.ContainsKey(key))
                {
                    Console.WriteLine(
                        $"\rFound override for {key}! Was this intentional?".PadRight(Console.WindowWidth - 2));
                    continue;
                }

                langJson.Add(key, line.Split("=")[1]);
            }

            File.WriteAllText(DataSources.ProjectAssetsDir + $"/lang/{toLang(new FileInfo(lang).Name)}.json",
                langJson.ToJsonString(new JsonSerializerOptions() {WriteIndented = true}));
            Console.WriteLine();
        }
    }

    private static string toLang(string name) => name switch
    {
        "en" => "en_us",
        _ => name.Replace("-", "_")
    };

    private static string getKey(string category, string key)
    {
        key = key.Replace("-", "_");
        category = category.Replace("-", "_");
        if (category.StartsWith("entity_")) category.Replace("entity_", "block_");
        if (category == "entity_name") return $"block.factoriocraft.{key}";
        if (category == "entity_description") return $"block.factoriocraft.{key}.description";
        if (category == "controls") return $"{category}.factoriocraft.{key}";
        if (category == "programmable_speaker_note") return $"sound.factoriocraft.programmable_speaker.{key}";
        if (category == "programmable_speaker_instrument") return $"sound.factoriocraft.{key}";
        if (category == "item_limitation") return $"error.factoriocraft.{key}";
        if (category == "autoplace_control_names") return $"controls.factoriocraft.autoplace.{key}";
        if (category.EndsWith("_name")) return $"{category.Replace("_name", "")}.factoriocraft.{key}";
        if (category.EndsWith("_description"))
            return $"{category.Replace("_description", "")}.factoriocraft.{key}.description";


        //defaults
        if (category == "shortcut" ||
            category == "story")
            return $"{category}.factoriocraft.{key}";

        Console.WriteLine($"\rUnknown category format: '{category}' + '{key}'".PadRight(Console.WindowWidth - 2));
        return $"{category}.factoriocraft.{key}";
    }
}