using System.Text.Json;
using System.Text.Json.Nodes;
using DataCodeGen.Classes;

namespace DataCodeGen;

public class ResearchGenerator
{
    public static void Run()
    {
        var head = new StreamWriter(File.OpenWrite("out/research_head.kt"));
        var body = new StreamWriter(File.OpenWrite("out/research_body.kt"));
        Dictionary<string, Research> research = new();
        while (research.Count < DataSources.JsonDump["technology"].AsObject().Count)
        {
            //Console.WriteLine($"{research.Count} -> {DataSources.JsonDump["technology"].AsObject().Count}");
            foreach (var tech in DataSources.JsonDump["technology"].AsObject())
            {
                if (research.ContainsKey(tech.Key)) continue;
                //tech.Value;
                if (tech.Value["prerequisites"] != null && !tech.Value["prerequisites"].AsArray().All(node => research.ContainsKey(node.ToString()))) continue;
                //Console.WriteLine($"{tech.Key} -> {tech.Value["prerequisites"]}");
                
                research.Add(tech.Key, (Research) tech.Value.Deserialize(typeof(Research)));
            }
            Console.Write($"\rAdding research: {research.Count}/{DataSources.JsonDump["technology"].AsObject().Count}");
        }
        Console.WriteLine();
        int i = 0;
        foreach (var (key, value) in research)
        {
            //AUTOMATION_RESEARCH = registerResearch(Research("automation", setOf(ROOT_RESEARCH), setOf(ItemStack(ItemRegistry.SCIENCE_PACK_ITEM, 1)), 10, 200), "automation")
            var deps = value.Prerequisites == null
                ? "setOf(ROOT_RESEARCH)"
                : $"setOf({string.Join(", ", value.Prerequisites.Select(ToVarName))})";
            var items = GetIngredients(value.Unit.Ingredients);
            var multiplier = value.Unit.Count;
            var ticks = value.Unit.Time * 20;
            head.WriteLine($"lateinit var {ToVarName(key)}: Research");
            body.WriteLine($"{ToVarName(key)} = registerResearch(Research(\"{key.Replace("-","_")}\", {deps}, {items}, {multiplier}, {ticks}), \"{key.Replace("-", "_")}\")");
            Console.Write($"\rGenerating code for research: {++i}/{DataSources.JsonDump["technology"].AsObject().Count}");
        }
        Console.WriteLine();
        head.Flush();
        head.Close();
        body.Flush();
        body.Close();
    }

    private static string ToVarName(string name)
    {
        return name.Replace("-", "_").ToUpper()+"_RESEARCH";
    }

    private static string GetIngredients(List<object[]> items)
    {
        return $"setOf({string.Join(", ", items.Select(x=>$"ItemStack(ItemRegistry.{x[0].ToString().Replace("-","_").ToUpper()}_ITEM, {x[1]})"))})";
    }
}