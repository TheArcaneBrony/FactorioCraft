using System.Text.Json.Serialization;

namespace DataCodeGen.Classes;


public class Research
{
    [JsonPropertyName("type")]
    public string Type { get; set; }

    [JsonPropertyName("name")]
    public string Name { get; set; }

    [JsonPropertyName("icon_size")]
    public int IconSize { get; set; }

    [JsonPropertyName("icon_mipmaps")]
    public int IconMipmaps { get; set; }

    [JsonPropertyName("icons")]
    public List<Icon> Icons { get; set; }

    [JsonPropertyName("effects")]
    public List<Effect> Effects { get; set; }

    [JsonPropertyName("prerequisites")]
    public List<string> Prerequisites { get; set; }

    [JsonPropertyName("unit")]
    public Unit Unit { get; set; }

    [JsonPropertyName("upgrade")]
    public bool Upgrade { get; set; }

    [JsonPropertyName("order")]
    public string Order { get; set; }
}


public class Icon
{
    [JsonPropertyName("icon")]
    public string IconPath { get; set; }

    [JsonPropertyName("icon_size")]
    public int IconSize { get; set; }

    [JsonPropertyName("icon_mipmaps")]
    public int IconMipmaps { get; set; }

    [JsonPropertyName("shift")]
    public List<int> Shift { get; set; }
}

public class Effect
{
    [JsonPropertyName("type")]
    public string Type { get; set; }

    [JsonPropertyName("ammo_category")]
    public string AmmoCategory { get; set; }

    [JsonPropertyName("modifier")]
    public object Modifier { get; set; }

    [JsonPropertyName("turret_id")]
    public string TurretId { get; set; }
}

public class Unit
{
    [JsonPropertyName("count")]
    public int Count { get; set; }

    [JsonPropertyName("ingredients")]
    public List<object[]> Ingredients { get; set; }

    [JsonPropertyName("time")]
    public int Time { get; set; }
}
