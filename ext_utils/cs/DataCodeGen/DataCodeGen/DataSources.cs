using System.Text.Json;
using System.Text.Json.Nodes;

namespace DataCodeGen;

public class DataSources
{
    public static string GameBaseDir =
        $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/.local/share/Steam/steamapps/common/Factorio/";

    public static string ProjectAssetsDir = ProjectRoot + "/src/main/resources/assets/factoriocraft/";
    public static string ProjectRoot
    {
        get
        {
            string dir = Environment.CurrentDirectory;
            while (!File.Exists(dir + "/build.gradle")) dir += "/..";
            return dir;
        }
    }

    public static JsonObject JsonDump =
        JsonSerializer.Deserialize<JsonObject>(File.ReadAllText(ProjectRoot + "/extra/factorio.json"))["raw"]
            .AsObject();
}