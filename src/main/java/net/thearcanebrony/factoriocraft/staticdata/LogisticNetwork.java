package net.thearcanebrony.factoriocraft.staticdata;

import net.thearcanebrony.factoriocraft.FactorioCraft;
import net.thearcanebrony.factoriocraft.block.entity.*;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;
import net.thearcanebrony.factoriocraft.entitiy.LogisticsRobotEntity;

import java.util.ArrayList;
import java.util.HashSet;

public class LogisticNetwork {
    public static HashSet<RoboportBlockEntity> roboports = new HashSet<>();
    public static ArrayList<ActiveProviderChestBlockEntity> activeProviderChests = new ArrayList<>();
    public static ArrayList<PassiveProviderChestBlockEntity> passiveProviderChests = new ArrayList<>();
    public static ArrayList<RequesterChestBlockEntity> requesterChests = new ArrayList<>();
    public static ArrayList<StorageChestBlockEntity> storageChests = new ArrayList<>();

    public static HashSet<BaseRobotEntity> robots = new HashSet<>();
    public static HashSet<LogisticsRobotEntity> logisticRobots = new HashSet<>();

    public int getNextId() {
        for (int i = 0; i <= robots.size(); i++) {
            int finalI = i;
            if (robots.stream().noneMatch(it -> it.getRobotId() == finalI)) return i;
        }
        return -1;
    }

    public static void addRobot(BaseRobotEntity robot) {
        FactorioCraft.LOGGER.info("adding robot {}", robot);
        robots.add(robot);
        if (robot instanceof LogisticsRobotEntity) {
            logisticRobots.add((LogisticsRobotEntity) robot);
        }
    }

    public static void removeRobot(BaseRobotEntity robot) {
        if (robots.contains(robot)) {
            FactorioCraft.LOGGER.info("removing robot {}", robot);
            robots.remove(robot);
            if (robot instanceof LogisticsRobotEntity) {
                logisticRobots.remove((LogisticsRobotEntity) robot);
            }
        }
    }

    public static void clear() {
        robots.clear();
        logisticRobots.clear();
    }
}
