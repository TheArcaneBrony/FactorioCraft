package net.thearcanebrony.factoriocraft.staticdata;

import net.thearcanebrony.factoriocraft.state.FactorioCraftState;

public interface FactorioStateHolder {
    FactorioCraftState getFactorioState();

    void saveState();

    void loadState();
}
