package net.thearcanebrony.factoriocraft.server;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.loader.api.minecraft.DedicatedServerOnly;
import org.quiltmc.qsl.base.api.entrypoint.server.DedicatedServerModInitializer;

@DedicatedServerOnly
public class FactorioCraftServer implements DedicatedServerModInitializer {
    @Override
    public void onInitializeServer(ModContainer mod) {
        System.out.printf("%s\n", "hello duh");
    }
}
