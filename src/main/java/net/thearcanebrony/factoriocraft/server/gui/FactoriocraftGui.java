package net.thearcanebrony.factoriocraft.server.gui;

import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import static com.google.common.primitives.Ints.min;
import static net.minecraft.util.math.MathHelper.clamp;

public class FactoriocraftGui extends JPanel {
    static Graphics g = null;
    static Graphics2D g2 = (Graphics2D) g;
    static String lip, a = "";
    int i, avg = 0;
    ArrayList<Integer> pingtime = new ArrayList<>();
    JFrame frame;
    Timer t, t_ThreadChecker, t_avgcalc;
    Thread fgw;
    int scrollpos = 0;
    boolean running = true;

    public FactoriocraftGui() {
        super();
        addKeyListener(new Key());
        setFocusable(true);
        frame = new JFrame("FactorioCraft Debug UI");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.add(this);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
//                    frame.setTitle("Minecraft server - shutting down!");
                running = false;
                t.stop();
            }
        });

        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        frame.setSize(Math.min(gd.getDisplayMode().getWidth(), 1280), Math.min(gd.getDisplayMode().getHeight() / 2, 720));
        this.setSize(frame.getSize());
        t = new Timer(100, new Listener());
        fgw = new Thread(new ForegroundWorker(), "ForegroundWorker");

        fgw.start();
        t.start();
    }

    public void updateUI(Graphics g) {
        try {
            if (frame == null) {
                System.out.println("Frame is null!");
                return;
            }
            frame.setTitle("FactorioDebugUI - " + System.currentTimeMillis());
//            g = getGraphics();
            int i = 20;
//            System.out.printf("Drawing %s robots\n", LogisticNetwork.robots.size());
            g.setColor(new Color(0x999999));
            g.clearRect(0, 0, frame.getWidth(), frame.getHeight());
            g.setColor(new Color(0x0));
            g.drawString(String.format("Displaying robots: %s - %s", scrollpos, scrollpos + 10), 0, i);
            var iterator = LogisticNetwork.robots.iterator();
            for (int j = 0; j < min(scrollpos, LogisticNetwork.robots.size() - 1); j++) {
                iterator.next();
            }
            for (int j = scrollpos; j < scrollpos + 10; j++) {
                if (j >= LogisticNetwork.robots.size()) break;
                if (j < 0) j = 0;
                var rbt = iterator.next();
                g.drawString(String.format("%s: %s", rbt.getType(), rbt.getBlockPos()), 0, i += 20);
            }
            //noinspection UnusedAssignment
            g.drawString("-- end of list --", 0, i += 20);
            repaint();
        } catch (Exception ignored) {

        }
    }

    @Override
    public void paint(Graphics g) {
        updateUI(g);
        super.paint(g);
    }

    @Override
    public JRootPane getRootPane() {
        return frame.getRootPane();
//        return super.getRootPane();
    }


    public void paintComponent(Graphics g) {
        updateUI(g);
        super.paintComponent(g);
    }

    private class Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {

            updateUI();
            repaint();
        }
    }

    private class Key extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                scrollpos += 10;
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                scrollpos -= 10;
            }
            scrollpos = clamp(scrollpos, 0, LogisticNetwork.robots.size() - 1);
        }

//        public void keyReleased(KeyEvent e) {
//            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
//
//            }
//        }
    }

    private class ForegroundWorker implements Runnable {
        @Override
        public void run() {
            if (getRootPane() == null) {
                System.out.println("getRootPane is null");
                return;
            }
            g = getRootPane().getGraphics();
            while (running) {
                try {
                    updateUI();
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /*
    //        public void netping(Graphics g, String pip) {
//            Thread.currentThread().setName(Thread.currentThread().getName() + " P");
//            try {
//                Start = System.currentTimeMillis();
//                ping = InetAddress.getByName(pip).isReachable(500);
//                End = System.currentTimeMillis();
//            } catch (IOException f) {
//                System.out.println(f.getMessage());
//            }
//            Thread.currentThread().setName(Thread.currentThread().getName() + " D");
//            g.setColor(ping ? Color.green : Color.red);
//            draw16(g, ti);
//            drawRadar(g, ti);
//            drawDbg(g);
//        }

//        public void draw16(Graphics g, int i) {
//            g.fillRect(16 * (ti % 16), 150 + Math.round(ti / 16) * 16, 16, 16);
//            g.drawLine((int) Math.round(100 + (ti % 51)), 53 + ti / 51, (int) Math.round(100 + (ti % 51)),
//                    53 + ti / 51);
//        }
    public void drawRadar(Graphics g, int ti) {
            Color c = new Color(getForeground().getRed() - 10, getForeground().getGreen() - 10,
                    getForeground().getBlue() - 10, 10);
            g.fillArc(27, 27, 66, 66, (int) -(ti * 1.411764705882353), -10);
            g.setColor(c);
            g.fillArc(27, 27, 66, 66, 0, 360);
        }

        public void drawDbg(Graphics g) {
            g.setColor(getBackground());
            g.fillRect(60, 100, 1000, 25);
            g.fillRect(301, 10, 1000, 10);
            g.drawRect(100, 5, 50, 20);
            g.setColor(Color.black);
            int pingi = (int) (End - Start);
            g.drawString("   " + pingi + " ms  " + (float) 1000 / pingi + "/s", 60, 120);
            if (pingi > 0) {
                pingtime.add(1000 / pingi);
            } else ;

            g.fillRect(300, 10, avg, 10);

        }

     */

}