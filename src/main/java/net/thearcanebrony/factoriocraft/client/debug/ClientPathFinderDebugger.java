package net.thearcanebrony.factoriocraft.client.debug;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderContext;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec3d;
import net.thearcanebrony.factoriocraft.client.FactorioCraftClient;
import org.lwjgl.opengl.GL11;
import org.quiltmc.loader.api.minecraft.ClientOnly;

import java.util.ArrayList;
import java.util.List;

@ClientOnly
public class ClientPathFinderDebugger extends DrawableHelper {
	private static final List<BlockPos>           nodes    = new ArrayList<>();
	public static        ClientPathFinderDebugger instance = new ClientPathFinderDebugger();
//	private static final RenderLayer    PATH_DEBUG_LAYER = RenderLayers.getBlockLayer(Blocks.AIR.getDefaultState());

	private static synchronized ImmutableList<BlockPos> getOrSetNodes (List<BlockPos> newData) {
		if(newData == null) {
			return ImmutableList.copyOf(nodes);
		} else {
			while(nodes.size() > 200) nodes.remove(0);
			nodes.addAll(newData);
			return null;
		}
	}

	public static void fromBuffer (PacketByteBuf buffer) {
		var newNodes  = new ArrayList<BlockPos>();
		int botId = buffer.readInt();
		int nodeCount = buffer.readInt();
		for(int i = 0; i < nodeCount; i++){
			newNodes.add(BlockPos.fromLong(buffer.readLong()));
		}
		getOrSetNodes(newNodes);
//		FactorioCraft.LOGGER.info("Got {} nodes", nodeCount);
	}

	public final void drawPathDebug (WorldRenderContext context) {
		if(!FactorioCraftClient.Companion.isDbg()) return;
		MatrixStack             matrices = context.matrixStack();
		Camera                  camera   = context.camera();
		Vec3d                   pos      = camera.getPos();
		ImmutableList<BlockPos> nodes    = getOrSetNodes(null);
		if(nodes.size() > 1) {
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glEnable(GL11.GL_LINE_SMOOTH);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GlStateManager._disableDepthTest();

			matrices.push();
//			matrices.multiply(new Quaternion(Vec3f.POSITIVE_X, MathHelper.wrapDegrees(camera.getPitch()), true));
//			matrices.multiply(new Quaternion(Vec3f.POSITIVE_Y, MathHelper.wrapDegrees(camera.getYaw() + 180f), true));

			matrices.translate(-pos.x, -pos.y, -pos.z);

			BufferBuilder bufferBuilder = Tessellator.getInstance().getBufferBuilder();
//			RenderSystem.setShaderColor(0, 255, 0, 255);
			RenderSystem.setShader(GameRenderer::getPositionColorShader);

			bufferBuilder.begin(VertexFormat.DrawMode.DEBUG_LINES, VertexFormats.POSITION_COLOR);

			BlockPos previousNode = null;
			for(var node : nodes){
				Matrix4f matrix = matrices.peek().getModel();
				if(previousNode != null) {

					bufferBuilder.vertex(matrix, (float) (previousNode.getX() + .5), (float) (previousNode.getY() + .5), (float) (previousNode.getZ() + .5)).color(0xff_ff_ff_ff).next();
					bufferBuilder.vertex(matrix, (float) (node.getX() + .5), (float) (node.getY() + .5), (float) (node.getZ() + .5)).color(0xff_00_ff_00).next();

				}
				bufferBuilder.vertex(matrix, (float) (node.getX() + .4), (float) (node.getY() + .4), (float) (node.getZ() + .4)).color(0xff_ff_00_00).next();
				bufferBuilder.vertex(matrix, (float) (node.getX() + .6), (float) (node.getY() + .6), (float) (node.getZ() + .6)).color(0xff_ff_00_00).next();

				bufferBuilder.vertex(matrix, (float) (node.getX() + .6), (float) (node.getY() + .4), (float) (node.getZ() + .4)).color(0xff_ff_00_00).next();
				bufferBuilder.vertex(matrix, (float) (node.getX() + .4), (float) (node.getY() + .6), (float) (node.getZ() + .6)).color(0xff_ff_00_00).next();

				bufferBuilder.vertex(matrix, (float) (node.getX() + .4), (float) (node.getY() + .6), (float) (node.getZ() + .4)).color(0xff_ff_00_00).next();
				bufferBuilder.vertex(matrix, (float) (node.getX() + .6), (float) (node.getY() + .4), (float) (node.getZ() + .6)).color(0xff_ff_00_00).next();

				bufferBuilder.vertex(matrix, (float) (node.getX() + .6), (float) (node.getY() + .6), (float) (node.getZ() + .4)).color(0xff_ff_00_00).next();
				bufferBuilder.vertex(matrix, (float) (node.getX() + .4), (float) (node.getY() + .4), (float) (node.getZ() + .6)).color(0xff_ff_00_00).next();
				previousNode = node;
//				matrices.translate(.0, .001, .0);
			}

			BufferRenderer.draw(bufferBuilder.end());

			matrices.pop();
			RenderSystem.setShaderColor(1, 1, 1, 1);
			GlStateManager._enableDepthTest();
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_LINE_SMOOTH);
		}
	}
}
