package net.thearcanebrony.factoriocraft.client.gui.hud

import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawableHelper
import net.minecraft.client.util.math.MatrixStack

object FactorioHud {
    fun Render(matrixStack: MatrixStack?) {
        val client = MinecraftClient.getInstance()
        var guiScale = client.window.scaleFactor.toFloat()
        val renderer = client.textRenderer
        val topRightX = client.window.width
        val topRightY = 0

        val startX = (topRightX - 256)
        val endY = 360

        matrixStack!!.scale(1 / guiScale, 1 / guiScale, 1f)
        //draw bg
        DrawableHelper.fill(matrixStack, startX, 0, topRightX, endY, 0xFF313031.toInt())
        //research button
        renderBorder(matrixStack, (startX + 7), 7, (startX + 248), 49)
        renderButtonBorder(matrixStack, (startX + 8), 9, (startX + 247), 47)

        renderText(matrixStack, (startX + 58), 25, "Press O to start a new research.", 0xFFFFFFFF, 1f)

        //menu buttons
        renderBorder(matrixStack, (startX + 7), 60, (startX + 248), 100)
        for (i in 0..5) {
            renderButtonBorder(matrixStack, (startX + 8 + (i * 39) + i), 62, (startX + 8 + ((i + 1) * 39) + i), 98)
        }

        //text
        client.textRenderer.draw(matrixStack, "yes!", 0f, 0f, 0xFFFFFFFF.toInt())


        //draw map
        renderBorder(matrixStack, (startX + 7), 110, (startX + 248), 110+240)
        DrawableHelper.fill(matrixStack, startX+8, 111, startX + 8 + 240, 111+238, 0xFF000000.toInt())
        DrawableHelper.fill(matrixStack, startX+16, 111+8, startX + 8 + 240-8, 111+238-8, 0xFFAAAAAA.toInt())
        MapRenderer.renderMiniMap(matrixStack, startX+16, 111+8)


        matrixStack.scale(guiScale, guiScale, 1f)
    }

    fun renderBorder(matrixStack: MatrixStack?, x1: Int, y1: Int, x2: Int, y2: Int) {
        //var posMatrix = matrixStack.peek().getPositionMatrix()

        val client = MinecraftClient.getInstance()
        var guiScale = client.window.scaleFactor.toFloat()
        //fill(matrixStack, x1, y1, x2, y2, 0xFFFF00FF.toInt()) //debug
        DrawableHelper.fill(matrixStack, x1, y1, x2, y1 + 1, 0xFF1A1817.toInt()) //top
        DrawableHelper.fill(matrixStack, x1, y2, x2, y2 + 1, 0xFF605E5D.toInt()) //bottom
        DrawableHelper.fill(matrixStack, x1, y1, x1 + 1, y2, 0xFF251E1C.toInt()) //left
        DrawableHelper.fill(matrixStack, x2, y1, x2 + 1, y2, 0xFF251E1C.toInt()) //right
    }

    fun renderButtonBorder(matrixStack: MatrixStack?, x1: Int, y1: Int, x2: Int, y2: Int) {
        //var posMatrix = matrixStack.peek().getPositionMatrix()

        val client = MinecraftClient.getInstance()
        var guiScale = client.window.scaleFactor.toFloat()
        //fill(matrixStack, x1, y1, x2, y2, 0xFFFF00FF.toInt()) //debug
        DrawableHelper.fill(matrixStack, x1, y1, x2, y1 + 1, 0xFF605E5D.toInt()) //top
        DrawableHelper.fill(matrixStack, x1, y2, x2, y2 + 1, 0xFF1A1817.toInt()) //bottom
        DrawableHelper.fill(matrixStack, x1, y1, x1 + 1, y2, 0xFF251E1C.toInt()) //left
        DrawableHelper.fill(matrixStack, x2, y1, x2 + 1, y2, 0xFF251E1C.toInt()) //right
    }

    fun renderText(matrixStack: MatrixStack?, x1: Int, y1: Int, text: String, color: Long, scale: Float) {
        matrixStack!!.scale(scale, scale, 1f);
        MinecraftClient.getInstance().textRenderer.draw(matrixStack, text, x1 /scale, y1/scale, color.toInt())
        matrixStack!!.scale(1/scale, 1/scale, 1f);
    }
}
