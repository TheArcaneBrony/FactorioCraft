package net.thearcanebrony.factoriocraft.client.gui.swing

import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork
import java.awt.BorderLayout
import javax.swing.*

class FactorioCraftDebugPanel : JComponent() {
	private val robotDataList: JList<BaseRobotEntity>
	private val tempModel: HashMapIntListModel<BaseRobotEntity>
	//	private val robotDataListModel = DefaultListModel<String>()
//	private val robotData = HashMap<Int, BaseRobotEntity>()
//	private val ids: HashSet<Int> = HashSet()

	/**
	 *
	 */
	fun updateDebugData() {
		setRobotData(LogisticNetwork.robots)
	}

	@Synchronized fun setRobotData(newData: HashSet<BaseRobotEntity>) {
		tempModel.replaceSet(newData)
	}

	init {
		layout = BorderLayout()
		val label = JLabel("Hello World")
		add(label, "North")
		tempModel = HashMapIntListModel(indexer = { it.robotId })
		robotDataList = JList(tempModel)
		robotDataList.selectionMode = ListSelectionModel.SINGLE_INTERVAL_SELECTION
		add(JScrollPane(robotDataList), "Center")
	}

	override fun updateUI() {
	}

}

class HashMapIntListModel<T>(val set: HashSet<T> = HashSet(), var indexer: (T) -> Int = { it.hashCode() }) : AbstractListModel<T>() {
	fun replaceSet(set: HashSet<T>, indexer: ((T) -> Int)? = null) {
		this.indexer = indexer?: this.indexer
		val knownValues = HashSet<Int>()
		for(item in set) if(indexer(item) in knownValues) throw IllegalStateException("indexer function should not give same index to different objects") else knownValues.add(indexer(item))
		this.set.also { it.clear() }.addAll(set)
		fireContentsChanged(this, 0, size)
	}

	override fun getSize() = set.size

	override fun getElementAt(index: Int): T = set.sortedBy(indexer)[index]
}
