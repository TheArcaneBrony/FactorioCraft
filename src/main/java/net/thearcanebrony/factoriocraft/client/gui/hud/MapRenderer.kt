package net.thearcanebrony.factoriocraft.client.gui.hud

import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.BufferRenderer
import com.mojang.blaze3d.vertex.Tessellator
import com.mojang.blaze3d.vertex.VertexFormat
import com.mojang.blaze3d.vertex.VertexFormats
import net.minecraft.client.gui.DrawableHelper
import net.minecraft.client.render.*
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.util.math.Matrix4f
import kotlin.random.Random

class MapRenderer {
    companion object {
        fun renderMiniMap(matrixStack: MatrixStack, x: Int, y: Int) {
            var pms = matrixStack.peek().model

            for (dx in 0..223){
                if(dx%2==1) continue
                for (dy in 0..223) {
                    if(dy%2==1) continue
                    DrawableHelper.fill(matrixStack, x+dx, y+dy, x+dx+1, y+dy+1,  Random.nextInt(0xFFFFFF) + 0xFF000000.toInt())
                }
            }
        }
        private fun setPixel(matrix: Matrix4f, x: Int, y: Int, color: Int) {
            val bufferBuilder = Tessellator.getInstance().bufferBuilder
            RenderSystem.enableBlend()
            RenderSystem.disableTexture()
            RenderSystem.defaultBlendFunc()
            RenderSystem.setShader { GameRenderer.getPositionColorShader() }
            bufferBuilder.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR)
            val x: Float = (color shr 24 and 255).toFloat()
            val a: Float = (color shr 24 and 255).toFloat() / 255.0f
            val r: Float = (color shr 16 and 255).toFloat() / 255.0f
            val g: Float = (color shr 8 and 255).toFloat() / 255.0f
            val b: Float = (color and 255).toFloat() / 255.0f
            bufferBuilder.vertex(matrix, x + 0, y + 0f, 0.0f).color(r, g, b, a).next()
            bufferBuilder.vertex(matrix, x + 0, y + 1f, 0.0f).color(r, g, b, a).next()
            bufferBuilder.vertex(matrix, x + 1, y + 0f, 0.0f).color(r, g, b, a).next()
            bufferBuilder.vertex(matrix, x + 1, y + 1f, 0.0f).color(r, g, b, a).next()
            BufferRenderer.draw(bufferBuilder.end())
            RenderSystem.enableTexture()
            RenderSystem.disableBlend()
        }
    }
}