package net.thearcanebrony.factoriocraft.client.gui.screen

import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.screen.ingame.HandledScreen
import net.minecraft.client.gui.widget.TextFieldWidget
import net.minecraft.client.resource.language.I18n
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.item.ItemStack
import net.minecraft.screen.PropertyDelegate
import net.minecraft.screen.ScreenHandler
import net.minecraft.screen.ScreenHandlerType
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.registry.Registry
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.client.gui.screen.ResearchScreen.*
import net.thearcanebrony.factoriocraft.registries.Technologies
import net.thearcanebrony.factoriocraft.research.Technology
import net.thearcanebrony.factoriocraft.state.ResearchState


class ResearchScreen(handler: ResearchScreenHandler, playerInventory: PlayerInventory, title: Text) : HandledScreen<ResearchScreenHandler>(handler, playerInventory, title) {

	val searchBox = TextFieldWidget(textRenderer, 0, 0, 100, 100, Text.translatable("abcasafdsa"))

	override fun drawBackground(matrices: MatrixStack?, delta: Float, mouseX: Int, mouseY: Int) {

	}

	override fun render(matrices: MatrixStack?, mouseX: Int, mouseY: Int, delta: Float) {
		renderBackground(matrices)
		super.render(matrices, mouseX, mouseY, delta)
		var y = 12.0

		try {
			for(res in Technologies.INSTANCE.toList().sortedWith(Comparator.comparing(Technology::getProgressLevel))) {
				var items = ""
				for(item in res.packs) items += "⅓${Registry.ITEM.getId(item.item)} ${item.count}, "
				val resText = "${I18n.translate(res.getTransKey())}: [$items ${res.researchTime / 20.0}s] x ${res.researchCount}"
				drawText(
					matrices, resText, 0, y.toInt(), when {
						handler.isResearched(res) -> 0xFFFFFFFF
						handler.isAvailable(res) -> 0xFF00FF00
						else -> 0xFF0000FF
					}
				)
				y += textRenderer.fontHeight
			}
		} catch(var15: Throwable) {
			FactorioCraft.LOGGER.error("something went wrong while sorting registry: ", var15)
		}
	}

	class ResearchScreenHandler(syncId: Int, val playerInventory: PlayerInventory) : ScreenHandler(null, syncId) {
		companion object {
			val researchScreenHandlerType = Registry.register(Registry.SCREEN_HANDLER, "research", ScreenHandlerType(::ResearchScreenHandler))
		}
		val researchItemList = DefaultedList.of<ItemStack>()

		val propertyDelegate: PropertyDelegate
		
		val researchState = ResearchState()

		init {
			propertyDelegate = object : PropertyDelegate {
				override fun get(index: Int): Int {
					return researchState.states[Technologies.INSTANCE.get(index)]!!
				}

				override fun set(index: Int, value: Int) {
					researchState.states[Technologies.INSTANCE.get(index)] = value
				}

				override fun size(): Int = Technologies.INSTANCE.size()

			}
			this.addProperties(propertyDelegate)
		}

		override fun quickTransfer(player: PlayerEntity?, fromIndex: Int): ItemStack {
			TODO("Not yet implemented")
		}

		override fun canUse(player: PlayerEntity?): Boolean = true

		fun isResearched(tech: Technology): Boolean = tech.parents.all {
			isResearched(it)
		} && propertyDelegate[Technologies.INSTANCE.getRawId(tech)] >= tech.researchCount * tech.researchTime

		fun isAvailable(res: Technology): Boolean = res.parents.all { isResearched(it) }
	}

	fun drawText(matrices: MatrixStack?, text: String, x: Int, y: Int, color: Long) {
		var targetX = x
		var targetY = y
		for(line in text.split("\n")) {
			if(targetY > MinecraftClient.getInstance().window.scaledHeight) return
			var currentIndex = 0
			while(currentIndex in line.indices) {
				val currentChar = line[currentIndex]
				if(currentChar == '⅓') {
					var itemIdentifierString: String
					val endPos = line.indexOf(' ', currentIndex + 1) - 1
					itemIdentifierString = line.substring(currentIndex + 1..endPos)
					itemRenderer.renderInGui(ItemStack(Registry.ITEM.get(Identifier.tryParse(itemIdentifierString)?:Identifier("barrier"))), targetX - 2, targetY - textRenderer.fontHeight / 2 - 2)
					targetX += 10
					currentIndex = endPos
				} else {
					val next = line.indexOf('⅓', currentIndex)
					textRenderer.draw(matrices, line.substring(currentIndex..next), targetX.toFloat(), targetY.toFloat(), color.toInt())
					targetX += textRenderer.getWidth(line.substring(currentIndex..next))
					currentIndex = next
				}
			}
			targetX = x
			targetY += textRenderer.fontHeight
		}
	}

	override fun handledScreenTick() {
		
	}
}