package net.thearcanebrony.factoriocraft.client

import com.mojang.blaze3d.platform.InputUtil
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry
import net.fabricmc.fabric.api.client.rendering.v1.HudRenderCallback
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderContext
import net.fabricmc.fabric.api.client.rendering.v1.WorldRenderEvents
import net.minecraft.client.MinecraftClient
import net.minecraft.client.color.item.ItemColorProvider
import net.minecraft.client.gui.screen.ingame.HandledScreens
import net.minecraft.client.network.ClientPlayNetworkHandler
import net.minecraft.client.option.KeyBind
import net.minecraft.client.render.entity.model.EntityModelLayer
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.network.PacketByteBuf
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket
import net.minecraft.network.packet.c2s.play.PlayerInputC2SPacket
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.client.debug.ClientPathFinderDebugger
import net.thearcanebrony.factoriocraft.client.gui.hud.FactorioHud
import net.thearcanebrony.factoriocraft.client.gui.screen.ResearchScreen
import net.thearcanebrony.factoriocraft.client.gui.swing.FactorioCraftDebugPanel
import net.thearcanebrony.factoriocraft.item.SciencePackItem
import net.thearcanebrony.factoriocraft.registries.EntityRendererRegistry.Companion.registerRenderers
import net.thearcanebrony.factoriocraft.registries.ItemRegistry
import org.lwjgl.glfw.GLFW
import org.quiltmc.loader.api.ModContainer
import org.quiltmc.loader.api.minecraft.ClientOnly
import org.quiltmc.qsl.base.api.entrypoint.client.ClientModInitializer
import org.quiltmc.qsl.lifecycle.api.client.event.ClientTickEvents
import org.quiltmc.qsl.networking.api.PacketSender
import org.quiltmc.qsl.networking.api.client.ClientPlayNetworking
import javax.swing.JFrame
import javax.swing.WindowConstants

@ClientOnly
class FactorioCraftClient : ClientModInitializer {
	private lateinit var keyBinding: KeyBind
	private lateinit var toggleDebugViewBinding: KeyBind
	private lateinit var toggleGuiBinding: KeyBind

	override fun onInitializeClient(mod: ModContainer?) {
//		createDebugWindow()

		//robot renderers
		registerRenderers()

		//register debug channels
		ClientPlayNetworking.registerGlobalReceiver(
			FactorioCraft.DEBUG_PATHFINDING_CHANNEL
		) { client: MinecraftClient, handler: ClientPlayNetworkHandler, buf: PacketByteBuf, responseSender: PacketSender -> handlePathfindingDebugData(client, handler, buf, responseSender) }
		//register debug renderers
		WorldRenderEvents.END.register(WorldRenderEvents.End { context: WorldRenderContext? -> ClientPathFinderDebugger.instance.drawPathDebug(context) })

		keyBinding = KeyBindingHelper.registerKeyBinding(
			KeyBind(
				"Open research GUI",  // The translation key of the keybinding's name
				InputUtil.Type.KEYSYM,  // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
				GLFW.GLFW_KEY_O,  // The keycode of the key
				"category.factoriocraft.keybinds" // The translation key of the keybinding's category.
			)
		)
		toggleDebugViewBinding = KeyBindingHelper.registerKeyBinding(
			KeyBind(
				"Toggle debug view",  // The translation key of the keybinding's name
				InputUtil.Type.KEYSYM,  // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
				GLFW.GLFW_KEY_F4,  // The keycode of the key
				"category.factoriocraft.keybinds" // The translation key of the keybinding's category.
			)
		)
		toggleGuiBinding = KeyBindingHelper.registerKeyBinding(
			KeyBind(
				"Toggle GUI",  // The translation key of the keybinding's name
				InputUtil.Type.KEYSYM,  // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
				GLFW.GLFW_KEY_F6,  // The keycode of the key
				"category.factoriocraft.keybinds" // The translation key of the keybinding's category.
			)
		)
		HudRenderCallback.EVENT.register(HudRenderCallback { matrixStack: MatrixStack?, tickDelta: Float ->
			//System.out.println(tickDelta)
			if(isGuiVisible) FactorioHud.Render(matrixStack)
		})
		ClientTickEvents.END.register(ClientTickEvents.End { client: MinecraftClient ->
			while(toggleDebugViewBinding.wasPressed()) {
				isDbg = when (isDbg) {
					true -> {
//						synchronized(FactorioCraft.debugPanel){
//							(FactorioCraft.debugPanel[0]?.parent as JFrame?)?.dispose()
//						}
						false
					}
					false -> {
//						createDebugWindow()
						true
					}
				}
			}
			while(keyBinding.wasPressed()) {
				HandledScreens.open(ResearchScreen.ResearchScreenHandler.researchScreenHandlerType, client, 0, Text.empty())
			}
			while(toggleGuiBinding.wasPressed()){
				isGuiVisible = !isGuiVisible
			}
		})

		ColorProviderRegistry.ITEM.register(
			ItemColorProvider { stack, tintIndex ->
				if(tintIndex == 0) (stack.item as SciencePackItem).type.color else -1
			}, ItemRegistry.AUTOMATION_SCIENCE_PACK_ITEM, ItemRegistry.CHEMICAL_SCIENCE_PACK_ITEM, ItemRegistry.LOGISTIC_SCIENCE_PACK_ITEM,
			ItemRegistry.MILITARY_SCIENCE_PACK_ITEM, ItemRegistry.PRODUCTION_SCIENCE_PACK_ITEM, ItemRegistry.SPACE_SCIENCE_PACK_ITEM,
			ItemRegistry.UTILITY_SCIENCE_PACK_ITEM
		)
	}

	@Suppress("unused")
	private fun createDebugWindow() {
		synchronized(FactorioCraft.debugPanel) {
			if(FactorioCraft.debugPanel[0] != null) {
				FactorioCraft.LOGGER.info("FactorioCraft Debug Window already exists, not creating new one")
				return
			}
		}
		Thread({
			val start = System.getProperty("java.awt.headless")
			FactorioCraft.LOGGER.info("Creating FactorioCraft Debug Window")
			try {
				System.setProperty("java.awt.headless", "false")
				val frame: JFrame = object : JFrame("FactorioDebug") {
					override fun dispose() {
						super.dispose()
						FactorioCraft.debugPanel[0] = null
					}
				}
				FactorioCraft.debugPanel[0] = FactorioCraftDebugPanel()
				frame.add(FactorioCraft.debugPanel[0], "Center")
				frame.setSize(854, 480)
				frame.defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
				frame.isVisible = true
				FactorioCraft.LOGGER.info("FactorioCraft Debug Window created")
			} catch(exception: Exception) {
				FactorioCraft.LOGGER.info("Failed to create FactorioCraft Debug Window", exception)
			} finally {
				System.setProperty("java.awt.headless", start)
			}
		}, "Debug GUI").start()
	}

	companion object {
		var isDbg: Boolean = false
		var isGuiVisible: Boolean = false
		@JvmStatic
		var MODEL_CUBE_LAYER: EntityModelLayer = EntityModelLayer(Identifier(FactorioCraft.ModId, "logistic_robot"), "main")

		private fun handlePathfindingDebugData(client: MinecraftClient, handler: ClientPlayNetworkHandler, buf: PacketByteBuf, responseSender: PacketSender) {
			ClientPathFinderDebugger.fromBuffer(buf)
		}
	}
}