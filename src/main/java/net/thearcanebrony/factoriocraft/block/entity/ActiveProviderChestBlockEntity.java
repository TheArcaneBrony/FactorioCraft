package net.thearcanebrony.factoriocraft.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.GenericInventory;
import net.thearcanebrony.factoriocraft.registries.BlockEntityRegistry;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

public class ActiveProviderChestBlockEntity extends BlockEntity implements GenericInventory, NamedScreenHandlerFactory {
    public ActiveProviderChestBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityRegistry.ACTIVE_PROVIDER_CHEST_BLOCK_ENTITY, pos, state);
        System.out.println(pos.getX());
    }
    public void tick (World world, BlockPos pos, BlockState state, ActiveProviderChestBlockEntity blockEntity){
        if(!(world.isClient || LogisticNetwork.activeProviderChests.contains(blockEntity))){
            LogisticNetwork.activeProviderChests.add(blockEntity);
            System.out.printf("Added active provider chest at (%s)\n", pos.toShortString());
        }
    }
    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(6*9, ItemStack.EMPTY);

    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
//        return new InventoryScreen6x9Handler(syncId, playerInventory, this);
        return GenericContainerScreenHandler.createGeneric9x6(syncId, playerInventory, this);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        Inventories.readNbt(nbt, this.items);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, this.items);
    }
}
