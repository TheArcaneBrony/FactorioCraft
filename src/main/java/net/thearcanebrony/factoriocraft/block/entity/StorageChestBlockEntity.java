package net.thearcanebrony.factoriocraft.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.GenericContainerScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.GenericInventory;
import net.thearcanebrony.factoriocraft.registries.BlockEntityRegistry;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

import java.util.List;

public class StorageChestBlockEntity extends BlockEntity implements GenericInventory, NamedScreenHandlerFactory/*, BlockEntityTicker<RoboportBlockEntity>*/ {
    public StorageChestBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityRegistry.STORAGE_CHEST_BLOCK_ENTITY, pos, state);
//        System.out.println(pos.getX());
    }

    public void tick (World world, BlockPos pos, BlockState state, StorageChestBlockEntity blockEntity){
        if(!(world.isClient || LogisticNetwork.storageChests.contains(blockEntity))){
            LogisticNetwork.storageChests.add(blockEntity);
            System.out.printf("Added storage chest at (%s)\n", pos.toShortString());
        }
    }

    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(54, ItemStack.EMPTY);

    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }

    public boolean canTakeAny (List<ItemStack> stacks){
        for(var stack : stacks){
//            if(offer(stack.copy())) return true;
            return true;
        }
        return false;
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
//        return new InventoryScreen6x9Handler(syncId, playerInventory, this);
        return GenericContainerScreenHandler.createGeneric9x6(syncId, playerInventory, this);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        Inventories.readNbt(nbt, this.items);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, this.items);
    }

    public void transfer (DefaultedList<ItemStack> items) {
        for(var item : items){
//            insertStack(-1, item);
        }
    }
}
