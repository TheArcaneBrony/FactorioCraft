package net.thearcanebrony.factoriocraft.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.HopperScreenHandler;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.GenericInventory;
import net.thearcanebrony.factoriocraft.entitiy.LogisticRobotEntity;
import net.thearcanebrony.factoriocraft.item.LogisticRobotItem;
import net.thearcanebrony.factoriocraft.registries.BlockEntityRegistry;
import net.thearcanebrony.factoriocraft.registries.EntityRegistry;
import net.thearcanebrony.factoriocraft.registries.ItemRegistry;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

import java.util.List;

public class RoboportBlockEntity extends BlockEntity implements GenericInventory, NamedScreenHandlerFactory/*, BlockEntityTicker<RoboportBlockEntity>*/ {
    public RoboportBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityRegistry.ROBOPORT_BLOCK_ENTITY, pos, state);
//        System.out.println(pos.getX());
    }

    public void tick(World world, BlockPos pos, BlockState state, RoboportBlockEntity blockEntity) {
        if (!world.isClient) {
            if (!LogisticNetwork.roboports.contains(blockEntity)) {
                LogisticNetwork.roboports.add(blockEntity);
                System.out.printf("Added roboport at (%s)\n", pos.toShortString());
            }
            if (state.get(Properties.ENABLED) && this.count(ItemRegistry.LOGISTIC_ROBOT_ITEM) > 0) {
                for (ItemStack stack : stacks) {
                    if (stack.getItem() instanceof LogisticRobotItem) {
                        stack.decrement(1);
                        var lre = new LogisticRobotEntity(EntityRegistry.LOGISTIC_ROBOT_ENTITY_TYPE, world);
                        lre.setPosition(Vec3d.ofCenter(pos.add(0, 1.5, 0)));
                        world.spawnEntity(lre);
                        break;
                    }
                }
            }
        }
    }

    private final DefaultedList<ItemStack> stacks = DefaultedList.ofSize(5, ItemStack.EMPTY);

    @Override
    public DefaultedList<ItemStack> getStacks() {
        return stacks;
    }

    public boolean canTakeAny(List<ItemStack> stacks) {
        for (var stack : stacks) {
//            if(offer(stack.copy())) return true;
            return true;
        }
        return false;
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        return stack.getItem() instanceof LogisticRobotItem;
//        return GenericInventory.super.isValid(slot, stack);
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory playerInventory, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
//        return new InventoryScreen5Handler(syncId, playerInventory, this);
        return new HopperScreenHandler(syncId, playerInventory, this);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());//.parse(null,null,null);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        Inventories.readNbt(nbt, this.stacks);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, this.stacks);
    }

    public void transfer(DefaultedList<ItemStack> stacks) {
        for (var stack : stacks) {
            for (int i = 0; i < this.stacks.size(); i++) {
                if (this.stacks.get(i).isEmpty()) {
                    this.stacks.set(i, stack);
                } else if (this.stacks.get(i).isItemEqual(stack)) {
                    while (this.stacks.get(i).getCount() < this.stacks.get(i).getMaxCount() && stack.getCount() > 0) {
                        this.stacks.get(i).increment(1);
                        stack.decrement(1);
                    }
                }
            }
//            insertStack(-1, stack);
        }
    }

    @Override
    public void markRemoved() {
        super.markRemoved();
        LogisticNetwork.roboports.remove(this);
    }

    public boolean isFullOfRobots() {
        var count = 0;
        for (var stack : stacks) {
            count += stack.getCount();
        }
        return count >= 350;
    }

    public void tryEnterRoboport(Entity entity, boolean hasPower) {
        if (isFullOfRobots()) return;
        for (var stack : stacks) {
            if (stack.getCount() < stack.getMaxCount()) {
                entity.stopRiding();
                entity.removeAllPassengers();
                NbtCompound nbtCompound = new NbtCompound();
                entity.saveNbt(nbtCompound);
                transfer(DefaultedList.copyOf(ItemStack.EMPTY, new ItemStack(ItemRegistry.LOGISTIC_ROBOT_ITEM, 1)));
//                this.addBee(nbtCompound, ticksInHive, hasNectar);
                if (this.world != null) {

                    BlockPos beeEntity = this.getPos();
                    this.world.playSound(null, beeEntity.getX(), beeEntity.getY(), beeEntity.getZ(), SoundEvents.BLOCK_BEEHIVE_ENTER, SoundCategory.BLOCKS, 1.0F, 1.0F);
                }

                entity.discard();
                super.markDirty();
                break;
            }
        }
    }
}
