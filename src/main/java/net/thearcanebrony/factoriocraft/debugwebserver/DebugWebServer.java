package net.thearcanebrony.factoriocraft.debugwebserver;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.MinecraftServer;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import java.io.IOException;

@Environment(EnvType.CLIENT)
public class DebugWebServer {
    public static void Start() {
        // Create and configure a ThreadPool.
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setName("server");

        // Create a Server instance.
        Server server = new Server(threadPool);

        // Create a ServerConnector to accept connections from clients.
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8081);

        // Add the Connector to the Server
        server.addConnector(connector);

        // Set a simple Handler to handle requests/responses.
        server.setHandler(new AbstractHandler() {
            @Override
            public void handle(String target, Request jettyRequest, HttpServletRequest request, HttpServletResponse response) {
//                System.out.println(target);
                // Mark the request as handled so that it
                // will not be processed by other handlers.
                jettyRequest.setHandled(true);
                try {
                    var outputStream = response.getOutputStream();
                    outputStream.print("<head><style>body{background-color:#000;color:#fff;}</style><meta http-equiv=\"refresh\" content=\"1\"></head>\n");
                    MinecraftServer srv = null;
                    if (MinecraftClient.getInstance() != null) {
                        if (MinecraftClient.getInstance().isIntegratedServerRunning()) {
                            srv = MinecraftClient.getInstance().getServer();
                        }
                    }

                    if (srv != null) {
                        outputStream.println("<script>");
                        outputStream.print("var yes = {");
                        for (var i : srv.lastTickLengths) outputStream.print(i + ", ");
                        outputStream.println("};");
                        outputStream.println("</script>");
                    }

                    synchronized (LogisticNetwork.robots) {
                        for (var bot : LogisticNetwork.robots) {
                            outputStream.println(String.format("%s (%.1f#%d): %s -> %s<br>", bot.getRobotType(), bot.getHealth(), bot.getRobotId(), bot.getBlockPos(), bot.getTargetPos()));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        // Start the Server so it starts accepting connections from clients.
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
