package net.thearcanebrony.factoriocraft.item

import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import kotlin.math.roundToInt

class SciencePackItem(val type: SciencePackType) : Item(Settings()) {
	companion object {
		val FILL_LEVEL = "FillLevel"
	}

	override fun getItemBarStep(stack: ItemStack): Int {
		val nbt = stack.orCreateNbt
		return if(FILL_LEVEL in nbt) (nbt.getFloat(FILL_LEVEL) * 13).roundToInt() else 13
	}

	override fun getItemBarColor(stack: ItemStack): Int = (stack.item as SciencePackItem).type.color

	override fun isItemBarVisible(stack: ItemStack): Boolean = FILL_LEVEL in stack.orCreateNbt
}

enum class SciencePackType(val color: Int = -1) {
	Automation(0xc92d2d),
	Chemical(0x2cb3d5),
	Logistic(0x2FC832),
	Military(0x474A56),
	Production(0xA22DD7),
	Space(0xCBCBCA),
	Utility(0xD6B02C);
}
