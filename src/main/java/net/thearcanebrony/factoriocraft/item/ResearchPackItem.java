package net.thearcanebrony.factoriocraft.item;

import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ResearchPackItem {
    public String name = "Unknown research!";
    public Collection<ResearchPackItem> requiredResearch = new ArrayList<>();
    public HashMap<Item, Integer> researchItems = new HashMap<>();
    public long researchTimeMultiplier = 200L; //ticks

    public static ResearchPackItem newResearchItem(String name, ResearchPackItem requiredResearch, HashMap<Item, Integer> researchItems, Long researchTimeMultiplier) {
        return newResearchItem(name, List.of(new ResearchPackItem[]{requiredResearch}), researchItems, researchTimeMultiplier);
    }

    public static ResearchPackItem newResearchItem(String name, Collection<ResearchPackItem> requiredResearch, HashMap<Item, Integer> researchItems, Long researchTimeMultiplier) {
        var ri = new ResearchPackItem();
        if (name != null) ri.name = name;
        if (requiredResearch != null) ri.requiredResearch = requiredResearch;
        if (researchItems != null) ri.researchItems = researchItems;
        if (researchTimeMultiplier != null) ri.researchTimeMultiplier = researchTimeMultiplier;
        return ri;
    }

    public boolean canResearch() {
        if (requiredResearch.size() == 0) return true;
        for (var res : requiredResearch) {
            if (!res.canResearch()) return false;
        }
        return true;
    }


    @Override
    public String toString() {
        int maxRes = 0;
        for (var r : researchItems.values()) if(r > maxRes) maxRes = r;
        return String.format("%s (%s*%s, %s ticks, %ss)", name, maxRes, researchTimeMultiplier, maxRes* researchTimeMultiplier, maxRes* researchTimeMultiplier /20d);
    }
}
