package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.Material
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.block.*
import org.quiltmc.qsl.block.extensions.api.QuiltBlockSettings

class BlockRegistry {
	companion object {
		lateinit var ROBOPORT:               RoboportBlock
		lateinit var REQUESTER_CHEST:        RequesterChestBlock
		lateinit var PASSIVE_PROVIDER_CHEST: PassiveProviderChestBlock
		lateinit var ACTIVE_PROVIDER_CHEST:  ActiveProviderChestBlock
		lateinit var STORAGE_CHEST:          StorageChestBlock
		lateinit var RESEARCH_LAB:           ResearchLabBlock
		init {
			ROBOPORT = RoboportBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
			REQUESTER_CHEST = RequesterChestBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
			PASSIVE_PROVIDER_CHEST = PassiveProviderChestBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
			ACTIVE_PROVIDER_CHEST = ActiveProviderChestBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
			STORAGE_CHEST = StorageChestBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
			RESEARCH_LAB = ResearchLabBlock(QuiltBlockSettings.of(Material.METAL).strength(4.0f))
		}

		fun register() {
			//register blocks
			registerBlock(ROBOPORT, "roboport")
			registerBlock(REQUESTER_CHEST, "requester_chest")
			registerBlock(PASSIVE_PROVIDER_CHEST, "passive_provider_chest")
			registerBlock(ACTIVE_PROVIDER_CHEST, "active_provider_chest")
			registerBlock(STORAGE_CHEST, "storage_chest")
			registerBlock(RESEARCH_LAB, "research_lab")
		}

		fun registerBlock(block: Block, name: String) {
			registerBlock(block, name, ItemGroup.MISC)
		}

		fun registerBlock(block: Block, name: String, itemGroup: ItemGroup) {
			Registry.register(Registry.BLOCK, Identifier(FactorioCraft.ModId, name), block)
			ItemRegistry.registerItem(BlockItem(block, FabricItemSettings().group(itemGroup)), name)
		}
	}
}