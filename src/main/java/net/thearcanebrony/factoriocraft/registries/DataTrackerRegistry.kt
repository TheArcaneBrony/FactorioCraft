package net.thearcanebrony.factoriocraft.registries

import net.minecraft.entity.data.TrackedDataHandler
import net.minecraft.entity.data.TrackedDataHandlerRegistry
import net.minecraft.network.PacketByteBuf
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import org.quiltmc.qsl.entity.networking.api.tracked_data.QuiltTrackedDataHandlerRegistry

class DataTrackerRegistry {
	companion object {

		val NULLABLE_BLOCK_POS: TrackedDataHandler<BlockPos?> = object : TrackedDataHandler<BlockPos?> {
			override fun write(packetByteBuf: PacketByteBuf, blockPos: BlockPos?) {
				packetByteBuf.writeBoolean(blockPos == null)
				packetByteBuf.writeBlockPos(blockPos ?: BlockPos(Int.MIN_VALUE, Int.MIN_VALUE, Int.MIN_VALUE))
			}

			override fun read(packetByteBuf: PacketByteBuf): BlockPos? {
				val b = packetByteBuf.readBoolean()
				val t = packetByteBuf.readBlockPos()
				return if(b) null else t
			}

			override fun copy(blockPos: BlockPos?): BlockPos? {
				return blockPos
			}
		}

		fun register() {
			QuiltTrackedDataHandlerRegistry.register(Identifier("nullable_block_pos"), NULLABLE_BLOCK_POS)
		}
	}
}