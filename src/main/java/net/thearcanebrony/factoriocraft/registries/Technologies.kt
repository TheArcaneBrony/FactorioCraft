package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.event.registry.FabricRegistryBuilder
import net.fabricmc.fabric.api.event.registry.RegistryAttribute
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.minecraft.util.registry.SimpleRegistry
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.research.Technology

@Suppress("MemberVisibilityCanBePrivate", "unused", "SpellCheckingInspection") object Technologies {
	@JvmStatic
	val INSTANCE: SimpleRegistry<Technology> = FabricRegistryBuilder.createDefaulted(Technology::class.java, Identifier(FactorioCraft.ModId, "research"), Identifier(FactorioCraft.ModId, "root")).run {
		attribute(RegistryAttribute.PERSISTED)
		attribute(RegistryAttribute.MODDED)
		attribute(RegistryAttribute.SYNCED)
		buildAndRegister()
	}

	@JvmStatic
	val ROOT = registerResearch(Technology())

	private val automation = ItemStack(ItemRegistry.AUTOMATION_SCIENCE_PACK_ITEM, 1)
	private val logic = ItemStack(ItemRegistry.LOGISTIC_SCIENCE_PACK_ITEM, 1)
	private val militia = ItemStack(ItemRegistry.MILITARY_SCIENCE_PACK_ITEM, 1)
	private val chemical = ItemStack(ItemRegistry.CHEMICAL_SCIENCE_PACK_ITEM, 1)
	private val utility = ItemStack(ItemRegistry.UTILITY_SCIENCE_PACK_ITEM, 1)
	private val production = ItemStack(ItemRegistry.PRODUCTION_SCIENCE_PACK_ITEM, 1)
	private val space = ItemStack(ItemRegistry.SPACE_SCIENCE_PACK_ITEM, 1)

	val AUTOMATION = registerResearch(Technology("automation", setOf(ROOT), setOf(automation), 10, 200))
	val ELECTRONICS = registerResearch(Technology("electronics", setOf(AUTOMATION), setOf(automation), 30, 300))
	val LOGISTIC_SCIENCE_PACK = registerResearch(Technology("logistic_science_pack", setOf(ROOT), setOf(automation), 75, 100))
	val STEEL_PROCESSING = registerResearch(Technology("steel_processing", setOf(ROOT), setOf(automation), 50, 100))
	val STEEL_AXE = registerResearch(Technology("steel_axe", setOf(STEEL_PROCESSING), setOf(automation), 50, 600))
	val MILITARY = registerResearch(Technology("military", setOf(ROOT), setOf(automation), 10, 300))
	val MILITARY_2 = registerResearch(Technology("military_2", setOf(MILITARY, STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 20, 300))
	val FAST_INSERTER = registerResearch(Technology("fast_inserter", setOf(ELECTRONICS), setOf(automation), 30, 300))
	val LOGISTICS = registerResearch(Technology("logistics", setOf(ROOT), setOf(automation), 20, 300))
	val OPTICS = registerResearch(Technology("optics", setOf(ROOT), setOf(automation), 10, 300))
	val SOLAR_ENERGY = registerResearch(Technology("solar_energy", setOf(OPTICS, ELECTRONICS, STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 250, 600))
	val HEAVY_ARMOR = registerResearch(Technology("heavy_armor", setOf(MILITARY, STEEL_PROCESSING), setOf(automation), 30, 600))
	val GUN_TURRET = registerResearch(Technology("gun_turret", setOf(ROOT), setOf(automation), 10, 200))
	val ELECTRIC_ENERGY_DISTRIBUTION_1 = registerResearch(Technology("electric_energy_distribution_1", setOf(ELECTRONICS, STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 120, 600))
	val ADVANCED_MATERIAL_PROCESSING = registerResearch(Technology("advanced_material_processing", setOf(STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 75, 600))
	val ENGINE = registerResearch(Technology("engine", setOf(STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 100, 300))
	val LANDFILL = registerResearch(Technology("landfill", setOf(LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 50, 600))
	val LOGISTICS_2 = registerResearch(Technology("logistics_2", setOf(LOGISTICS, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 200, 600))
	val TOOLBELT = registerResearch(Technology("toolbelt", setOf(LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 100, 600))
	val STONE_WALL = registerResearch(Technology("stone_wall", setOf(ROOT), setOf(automation), 10, 200))
	val GATE = registerResearch(Technology("gate", setOf(STONE_WALL, MILITARY_2), setOf(automation, logic), 100, 600))
	val MILITARY_SCIENCE_PACK = registerResearch(Technology("military_science_pack", setOf(MILITARY_2, STONE_WALL), setOf(automation, logic), 30, 300))
	val DEFENDER = registerResearch(Technology("defender", setOf(MILITARY_SCIENCE_PACK), setOf(automation, logic, militia), 100, 600))
	val CIRCUIT_NETWORK = registerResearch(Technology("circuit_network", setOf(ELECTRONICS, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 100, 300))
	val PHYSICAL_PROJECTILE_DAMAGE_1 = registerResearch(Technology("physical_projectile_damage_1", setOf(MILITARY), setOf(automation), 100, 600))
	val PHYSICAL_PROJECTILE_DAMAGE_2 = registerResearch(Technology("physical_projectile_damage_2", setOf(PHYSICAL_PROJECTILE_DAMAGE_1), setOf(automation, logic), 200, 600))
	val WEAPON_SHOOTING_SPEED_1 = registerResearch(Technology("weapon_shooting_speed_1", setOf(MILITARY), setOf(automation), 100, 600))
	val WEAPON_SHOOTING_SPEED_2 = registerResearch(Technology("weapon_shooting_speed_2", setOf(WEAPON_SHOOTING_SPEED_1), setOf(automation, logic), 200, 600))
	val STRONGER_EXPLOSIVES_1 = registerResearch(Technology("stronger_explosives_1", setOf(MILITARY_2), setOf(automation, logic), 100, 600))
	val PHYSICAL_PROJECTILE_DAMAGE_3 = registerResearch(Technology("physical_projectile_damage_3", setOf(PHYSICAL_PROJECTILE_DAMAGE_2), setOf(automation, logic, militia), 300, 1200))
	val PHYSICAL_PROJECTILE_DAMAGE_4 = registerResearch(Technology("physical_projectile_damage_4", setOf(PHYSICAL_PROJECTILE_DAMAGE_3), setOf(automation, logic, militia), 400, 1200))
	val PHYSICAL_PROJECTILE_DAMAGE_5 = registerResearch(Technology("physical_projectile_damage_5", setOf(PHYSICAL_PROJECTILE_DAMAGE_4), setOf(automation, logic, chemical, militia), 500, 1200))
	val PHYSICAL_PROJECTILE_DAMAGE_6 = registerResearch(Technology("physical_projectile_damage_6", setOf(PHYSICAL_PROJECTILE_DAMAGE_5), setOf(automation, logic, chemical, militia, utility), 600, 1200))
	val STRONGER_EXPLOSIVES_2 = registerResearch(Technology("stronger_explosives_2", setOf(STRONGER_EXPLOSIVES_1), setOf(automation, logic, militia), 200, 600))
	val STRONGER_EXPLOSIVES_3 = registerResearch(Technology("stronger_explosives_3", setOf(STRONGER_EXPLOSIVES_2), setOf(automation, logic, militia, chemical), 300, 1200))
	val STRONGER_EXPLOSIVES_4 = registerResearch(Technology("stronger_explosives_4", setOf(STRONGER_EXPLOSIVES_3), setOf(automation, logic, militia, chemical, utility), 400, 1200))
	val STRONGER_EXPLOSIVES_5 = registerResearch(Technology("stronger_explosives_5", setOf(STRONGER_EXPLOSIVES_4), setOf(automation, logic, militia, chemical, utility), 500, 1200))
	val STRONGER_EXPLOSIVES_6 = registerResearch(Technology("stronger_explosives_6", setOf(STRONGER_EXPLOSIVES_5), setOf(automation, logic, chemical, militia, utility), 600, 1200))
	val WEAPON_SHOOTING_SPEED_3 = registerResearch(Technology("weapon_shooting_speed_3", setOf(WEAPON_SHOOTING_SPEED_2), setOf(automation, logic, militia), 300, 1200))
	val WEAPON_SHOOTING_SPEED_4 = registerResearch(Technology("weapon_shooting_speed_4", setOf(WEAPON_SHOOTING_SPEED_3), setOf(automation, logic, militia), 400, 1200))
	val WEAPON_SHOOTING_SPEED_5 = registerResearch(Technology("weapon_shooting_speed_5", setOf(WEAPON_SHOOTING_SPEED_4), setOf(automation, logic, chemical, militia), 500, 1200))
	val WEAPON_SHOOTING_SPEED_6 = registerResearch(Technology("weapon_shooting_speed_6", setOf(WEAPON_SHOOTING_SPEED_5), setOf(automation, logic, chemical, militia, utility), 600, 1200))
	val FOLLOWER_ROBOT_COUNT_1 = registerResearch(Technology("follower_robot_count_1", setOf(DEFENDER), setOf(automation, logic, militia), 200, 600))
	val FOLLOWER_ROBOT_COUNT_2 = registerResearch(Technology("follower_robot_count_2", setOf(FOLLOWER_ROBOT_COUNT_1), setOf(automation, logic, militia), 300, 600))
	val FOLLOWER_ROBOT_COUNT_3 = registerResearch(Technology("follower_robot_count_3", setOf(FOLLOWER_ROBOT_COUNT_2), setOf(automation, logic, chemical, militia), 400, 600))
	val FOLLOWER_ROBOT_COUNT_4 = registerResearch(Technology("follower_robot_count_4", setOf(FOLLOWER_ROBOT_COUNT_3), setOf(automation, logic, chemical, militia), 600, 600))
	val AUTOMATION_2 = registerResearch(Technology("automation_2", setOf(ELECTRONICS, STEEL_PROCESSING, LOGISTIC_SCIENCE_PACK), setOf(automation, logic), 40, 300))
	val RAILWAY = registerResearch(Technology("railway", setOf(LOGISTICS_2, ENGINE), setOf(automation, logic), 75, 600))
	val AUTOMATED_RAIL_TRANSPORTATION = registerResearch(Technology("automated_rail_transportation", setOf(RAILWAY), setOf(automation, logic), 75, 600))
	val RAIL_SIGNALS = registerResearch(Technology("rail_signals", setOf(AUTOMATED_RAIL_TRANSPORTATION), setOf(automation, logic), 100, 600))
	val AUTOMOBILISM = registerResearch(Technology("automobilism", setOf(LOGISTICS_2, ENGINE), setOf(automation, logic), 100, 600))
	val RESEARCH_SPEED_1 = registerResearch(Technology("research_speed_1", setOf(AUTOMATION_2), setOf(automation, logic), 100, 600))
	val RESEARCH_SPEED_2 = registerResearch(Technology("research_speed_2", setOf(RESEARCH_SPEED_1), setOf(automation, logic), 200, 600))
	val CONCRETE = registerResearch(Technology("concrete", setOf(ADVANCED_MATERIAL_PROCESSING, AUTOMATION_2), setOf(automation, logic), 250, 600))
	val RESEARCH_SPEED_3 = registerResearch(Technology("research_speed_3", setOf(RESEARCH_SPEED_2), setOf(automation, logic, chemical), 250, 600))
	val RESEARCH_SPEED_4 = registerResearch(Technology("research_speed_4", setOf(RESEARCH_SPEED_3), setOf(automation, logic, chemical), 500, 600))
	val RESEARCH_SPEED_5 = registerResearch(Technology("research_speed_5", setOf(RESEARCH_SPEED_4), setOf(automation, logic, chemical, production), 500, 600))
	val RESEARCH_SPEED_6 = registerResearch(Technology("research_speed_6", setOf(RESEARCH_SPEED_5), setOf(automation, logic, chemical, production, utility), 500, 600))
	val FLUID_HANDLING = registerResearch(Technology("fluid_handling", setOf(AUTOMATION_2, ENGINE), setOf(automation, logic), 50, 300))
	val OIL_PROCESSING = registerResearch(Technology("oil_processing", setOf(FLUID_HANDLING), setOf(automation, logic), 100, 600))
	val SULFUR_PROCESSING = registerResearch(Technology("sulfur_processing", setOf(OIL_PROCESSING), setOf(automation, logic), 150, 600))
	val PLASTICS = registerResearch(Technology("plastics", setOf(OIL_PROCESSING), setOf(automation, logic), 200, 600))
	val EXPLOSIVES = registerResearch(Technology("explosives", setOf(SULFUR_PROCESSING), setOf(automation, logic), 100, 300))
	val CLIFF_EXPLOSIVES = registerResearch(Technology("cliff_explosives", setOf(EXPLOSIVES, MILITARY_2), setOf(automation, logic), 200, 300))
	val FLAMMABLES = registerResearch(Technology("flammables", setOf(OIL_PROCESSING), setOf(automation, logic), 50, 600))
	val LAND_MINE = registerResearch(Technology("land_mine", setOf(EXPLOSIVES, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia), 100, 600))
	val FLAMETHROWER = registerResearch(Technology("flamethrower", setOf(FLAMMABLES, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia), 50, 600))
	val ADVANCED_ELECTRONICS = registerResearch(Technology("advanced_electronics", setOf(PLASTICS), setOf(automation, logic), 200, 300))
	val FLUID_WAGON = registerResearch(Technology("fluid_wagon", setOf(RAILWAY, FLUID_HANDLING), setOf(automation, logic), 200, 600))
	val ROCKETRY = registerResearch(Technology("rocketry", setOf(EXPLOSIVES, FLAMMABLES, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia), 120, 300))
	val MODULAR_ARMOR = registerResearch(Technology("modular_armor", setOf(HEAVY_ARMOR, ADVANCED_ELECTRONICS), setOf(automation, logic), 100, 600))
	val BATTERY = registerResearch(Technology("battery", setOf(SULFUR_PROCESSING), setOf(automation, logic), 150, 600))
	val SOLAR_PANEL_EQUIPMENT = registerResearch(Technology("solar_panel_equipment", setOf(MODULAR_ARMOR, SOLAR_ENERGY), setOf(automation, logic), 100, 300))
	val MODULES = registerResearch(Technology("modules", setOf(ADVANCED_ELECTRONICS), setOf(automation, logic), 100, 600))
	val SPEED_MODULE = registerResearch(Technology("speed_module", setOf(MODULES), setOf(automation, logic), 50, 600))
	val PRODUCTIVITY_MODULE = registerResearch(Technology("productivity_module", setOf(MODULES), setOf(automation, logic), 50, 600))
	val EFFECTIVITY_MODULE = registerResearch(Technology("effectivity_module", setOf(MODULES), setOf(automation, logic), 50, 600))
	val MINING_PRODUCTIVITY_1 = registerResearch(Technology("mining_productivity_1", setOf(ADVANCED_ELECTRONICS), setOf(automation, logic), 250, 1200))
	val MINING_PRODUCTIVITY_2 = registerResearch(Technology("mining_productivity_2", setOf(MINING_PRODUCTIVITY_1), setOf(automation, logic, chemical), 500, 1200))
	val MINING_PRODUCTIVITY_3 = registerResearch(Technology("mining_productivity_3", setOf(MINING_PRODUCTIVITY_2), setOf(automation, logic, chemical, production, utility), 1000, 1200))
	val REFINED_FLAMMABLES_1 = registerResearch(Technology("refined_flammables_1", setOf(FLAMETHROWER), setOf(automation, logic, militia), 100, 600))
	val REFINED_FLAMMABLES_2 = registerResearch(Technology("refined_flammables_2", setOf(REFINED_FLAMMABLES_1), setOf(automation, logic, militia), 200, 600))
	val REFINED_FLAMMABLES_3 = registerResearch(Technology("refined_flammables_3", setOf(REFINED_FLAMMABLES_2), setOf(automation, logic, militia, chemical), 300, 1200))
	val REFINED_FLAMMABLES_4 = registerResearch(Technology("refined_flammables_4", setOf(REFINED_FLAMMABLES_3), setOf(automation, logic, militia, chemical, utility), 400, 1200))
	val REFINED_FLAMMABLES_5 = registerResearch(Technology("refined_flammables_5", setOf(REFINED_FLAMMABLES_4), setOf(automation, logic, militia, chemical, utility), 500, 1200))
	val REFINED_FLAMMABLES_6 = registerResearch(Technology("refined_flammables_6", setOf(REFINED_FLAMMABLES_5), setOf(automation, logic, chemical, militia, utility), 600, 1200))
	val STACK_INSERTER = registerResearch(Technology("stack_inserter", setOf(FAST_INSERTER, LOGISTICS_2, ADVANCED_ELECTRONICS), setOf(automation, logic), 150, 600))
	val INSERTER_CAPACITY_BONUS_1 = registerResearch(Technology("inserter_capacity_bonus_1", setOf(STACK_INSERTER), setOf(automation, logic), 200, 600))
	val INSERTER_CAPACITY_BONUS_2 = registerResearch(Technology("inserter_capacity_bonus_2", setOf(INSERTER_CAPACITY_BONUS_1), setOf(automation, logic), 250, 600))
	val INSERTER_CAPACITY_BONUS_3 = registerResearch(Technology("inserter_capacity_bonus_3", setOf(INSERTER_CAPACITY_BONUS_2), setOf(automation, logic, chemical), 250, 600))
	val INSERTER_CAPACITY_BONUS_4 = registerResearch(Technology("inserter_capacity_bonus_4", setOf(INSERTER_CAPACITY_BONUS_3), setOf(automation, logic, chemical, production), 250, 600))
	val INSERTER_CAPACITY_BONUS_5 = registerResearch(Technology("inserter_capacity_bonus_5", setOf(INSERTER_CAPACITY_BONUS_4), setOf(automation, logic, chemical, production), 300, 600))
	val INSERTER_CAPACITY_BONUS_6 = registerResearch(Technology("inserter_capacity_bonus_6", setOf(INSERTER_CAPACITY_BONUS_5), setOf(automation, logic, chemical, production), 400, 600))
	val INSERTER_CAPACITY_BONUS_7 = registerResearch(Technology("inserter_capacity_bonus_7", setOf(INSERTER_CAPACITY_BONUS_6), setOf(automation, logic, chemical, production, utility), 600, 600))
	val CHEMICAL_SCIENCE_PACK = registerResearch(Technology("chemical_science_pack", setOf(ADVANCED_ELECTRONICS, SULFUR_PROCESSING), setOf(automation, logic), 75, 200))
	val MILITARY_3 = registerResearch(Technology("military_3", setOf(CHEMICAL_SCIENCE_PACK, MILITARY_SCIENCE_PACK), setOf(automation, logic, chemical, militia), 100, 600))
	val ADVANCED_ELECTRONICS_2 = registerResearch(Technology("advanced_electronics_2", setOf(CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 300, 600))
	val BRAKING_FORCE_1 = registerResearch(Technology("braking_force_1", setOf(RAILWAY, CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 100, 600))
	val BRAKING_FORCE_2 = registerResearch(Technology("braking_force_2", setOf(BRAKING_FORCE_1), setOf(automation, logic, chemical), 200, 600))
	val BRAKING_FORCE_3 = registerResearch(Technology("braking_force_3", setOf(BRAKING_FORCE_2), setOf(automation, logic, chemical, production), 250, 600))
	val BRAKING_FORCE_4 = registerResearch(Technology("braking_force_4", setOf(BRAKING_FORCE_3), setOf(automation, logic, chemical, production), 350, 600))
	val BRAKING_FORCE_5 = registerResearch(Technology("braking_force_5", setOf(BRAKING_FORCE_4), setOf(automation, logic, chemical, production), 450, 700))
	val BRAKING_FORCE_6 = registerResearch(Technology("braking_force_6", setOf(BRAKING_FORCE_5), setOf(automation, logic, chemical, production, utility), 550, 900))
	val BRAKING_FORCE_7 = registerResearch(Technology("braking_force_7", setOf(BRAKING_FORCE_6), setOf(automation, logic, chemical, production, utility), 650, 1200))
	val TANK = registerResearch(Technology("tank", setOf(AUTOMOBILISM, MILITARY_3, EXPLOSIVES), setOf(automation, logic, chemical, militia), 250, 600))
	val LASER = registerResearch(Technology("laser", setOf(OPTICS, BATTERY, CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 100, 600))
	val EXPLOSIVE_ROCKETRY = registerResearch(Technology("explosive_rocketry", setOf(ROCKETRY, MILITARY_3), setOf(automation, logic, chemical, militia), 100, 600))
	val LASER_TURRET = registerResearch(Technology("laser_turret", setOf(LASER, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia, chemical), 150, 600))
	val LOW_DENSITY_STRUCTURE = registerResearch(Technology("low_density_structure", setOf(ADVANCED_MATERIAL_PROCESSING, CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 300, 900))
	val ELECTRIC_ENERGY_DISTRIBUTION_2 = registerResearch(Technology("electric_energy_distribution_2", setOf(ELECTRIC_ENERGY_DISTRIBUTION_1, CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 100, 900))
	val ELECTRIC_ENERGY_ACCUMULATORS = registerResearch(Technology("electric_energy_accumulators", setOf(ELECTRIC_ENERGY_DISTRIBUTION_1, BATTERY), setOf(automation, logic), 150, 600))
	val ADVANCED_MATERIAL_PROCESSING_2 = registerResearch(Technology("advanced_material_processing_2", setOf(ADVANCED_MATERIAL_PROCESSING, CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 250, 600))
	val ENERGY_SHIELD_EQUIPMENT = registerResearch(Technology("energy_shield_equipment", setOf(SOLAR_PANEL_EQUIPMENT, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia), 150, 300))
	val NIGHT_VISION_EQUIPMENT = registerResearch(Technology("night_vision_equipment", setOf(SOLAR_PANEL_EQUIPMENT), setOf(automation, logic), 50, 300))
	val BELT_IMMUNITY_EQUIPMENT = registerResearch(Technology("belt_immunity_equipment", setOf(SOLAR_PANEL_EQUIPMENT), setOf(automation, logic), 50, 300))
	val BATTERY_EQUIPMENT = registerResearch(Technology("battery_equipment", setOf(BATTERY, SOLAR_PANEL_EQUIPMENT), setOf(automation, logic), 50, 300))
	val ADVANCED_OIL_PROCESSING = registerResearch(Technology("advanced_oil_processing", setOf(CHEMICAL_SCIENCE_PACK), setOf(automation, logic, chemical), 75, 600))
	val SPEED_MODULE_2 = registerResearch(Technology("speed_module_2", setOf(SPEED_MODULE, ADVANCED_ELECTRONICS_2), setOf(automation, logic, chemical), 75, 600))
	val PRODUCTIVITY_MODULE_2 = registerResearch(Technology("productivity_module_2", setOf(PRODUCTIVITY_MODULE, ADVANCED_ELECTRONICS_2), setOf(automation, logic, chemical), 75, 600))
	val EFFECTIVITY_MODULE_2 = registerResearch(Technology("effectivity_module_2", setOf(EFFECTIVITY_MODULE, ADVANCED_ELECTRONICS_2), setOf(automation, logic, chemical), 75, 600))
	val DISTRACTOR = registerResearch(Technology("distractor", setOf(DEFENDER, MILITARY_3, LASER), setOf(automation, logic, chemical, militia), 200, 600))
	val URANIUM_PROCESSING = registerResearch(Technology("uranium_processing", setOf(CHEMICAL_SCIENCE_PACK, CONCRETE), setOf(automation, logic, chemical), 200, 600))
	val NUCLEAR_POWER = registerResearch(Technology("nuclear_power", setOf(URANIUM_PROCESSING), setOf(automation, logic, chemical), 800, 600))
	val ENERGY_WEAPONS_DAMAGE_1 = registerResearch(Technology("energy_weapons_damage_1", setOf(LASER, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia, chemical), 100, 600))
	val ENERGY_WEAPONS_DAMAGE_2 = registerResearch(Technology("energy_weapons_damage_2", setOf(ENERGY_WEAPONS_DAMAGE_1), setOf(automation, logic, militia, chemical), 200, 600))
	val ENERGY_WEAPONS_DAMAGE_3 = registerResearch(Technology("energy_weapons_damage_3", setOf(ENERGY_WEAPONS_DAMAGE_2), setOf(automation, logic, militia, chemical), 300, 1200))
	val ENERGY_WEAPONS_DAMAGE_4 = registerResearch(Technology("energy_weapons_damage_4", setOf(ENERGY_WEAPONS_DAMAGE_3), setOf(automation, logic, militia, chemical), 400, 1200))
	val ENERGY_WEAPONS_DAMAGE_5 = registerResearch(Technology("energy_weapons_damage_5", setOf(ENERGY_WEAPONS_DAMAGE_4), setOf(automation, logic, chemical, militia, utility), 500, 1200))
	val ENERGY_WEAPONS_DAMAGE_6 = registerResearch(Technology("energy_weapons_damage_6", setOf(ENERGY_WEAPONS_DAMAGE_5), setOf(automation, logic, chemical, militia, utility), 600, 1200))
	val LASER_SHOOTING_SPEED_1 = registerResearch(Technology("laser_shooting_speed_1", setOf(LASER, MILITARY_SCIENCE_PACK), setOf(automation, logic, militia, chemical), 50, 600))
	val LASER_SHOOTING_SPEED_2 = registerResearch(Technology("laser_shooting_speed_2", setOf(LASER_SHOOTING_SPEED_1), setOf(automation, logic, militia, chemical), 100, 600))
	val LASER_SHOOTING_SPEED_3 = registerResearch(Technology("laser_shooting_speed_3", setOf(LASER_SHOOTING_SPEED_2), setOf(automation, logic, chemical, militia), 200, 1200))
	val LASER_SHOOTING_SPEED_4 = registerResearch(Technology("laser_shooting_speed_4", setOf(LASER_SHOOTING_SPEED_3), setOf(automation, logic, chemical, militia), 200, 1200))
	val LASER_SHOOTING_SPEED_5 = registerResearch(Technology("laser_shooting_speed_5", setOf(LASER_SHOOTING_SPEED_4), setOf(automation, logic, chemical, militia, utility), 200, 1200))
	val LASER_SHOOTING_SPEED_6 = registerResearch(Technology("laser_shooting_speed_6", setOf(LASER_SHOOTING_SPEED_5), setOf(automation, logic, chemical, militia, utility), 350, 1200))
	val LASER_SHOOTING_SPEED_7 = registerResearch(Technology("laser_shooting_speed_7", setOf(LASER_SHOOTING_SPEED_6), setOf(automation, logic, chemical, militia, utility), 450, 1200))
	val PRODUCTION_SCIENCE_PACK = registerResearch(Technology("production_science_pack", setOf(PRODUCTIVITY_MODULE, ADVANCED_MATERIAL_PROCESSING_2, RAILWAY), setOf(automation, logic, chemical), 100, 600))
	val AUTOMATION_3 = registerResearch(Technology("automation_3", setOf(SPEED_MODULE, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 150, 1200))
	val ROCKET_FUEL = registerResearch(Technology("rocket_fuel", setOf(FLAMMABLES, ADVANCED_OIL_PROCESSING), setOf(automation, logic, chemical), 300, 900))
	val EFFECT_TRANSMISSION = registerResearch(Technology("effect_transmission", setOf(ADVANCED_ELECTRONICS_2, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 75, 600))
	val LUBRICANT = registerResearch(Technology("lubricant", setOf(ADVANCED_OIL_PROCESSING), setOf(automation, logic, chemical), 50, 600))
	val ELECTRIC_ENGINE = registerResearch(Technology("electric_engine", setOf(LUBRICANT), setOf(automation, logic, chemical), 50, 600))
	val EXOSKELETON_EQUIPMENT = registerResearch(Technology("exoskeleton_equipment", setOf(ADVANCED_ELECTRONICS_2, ELECTRIC_ENGINE, SOLAR_PANEL_EQUIPMENT), setOf(automation, logic, chemical), 50, 600))
	val COAL_LIQUEFACTION = registerResearch(Technology("coal_liquefaction", setOf(ADVANCED_OIL_PROCESSING, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 200, 600))
	val SPEED_MODULE_3 = registerResearch(Technology("speed_module_3", setOf(SPEED_MODULE_2, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 300, 1200))
	val PRODUCTIVITY_MODULE_3 = registerResearch(Technology("productivity_module_3", setOf(PRODUCTIVITY_MODULE_2, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 300, 1200))
	val EFFECTIVITY_MODULE_3 = registerResearch(Technology("effectivity_module_3", setOf(EFFECTIVITY_MODULE_2, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 300, 1200))
	val KOVAREX_ENRICHMENT_PROCESS = registerResearch(Technology("kovarex_enrichment_process", setOf(PRODUCTION_SCIENCE_PACK, URANIUM_PROCESSING, ROCKET_FUEL), setOf(automation, logic, chemical, production), 1500, 600))
	val NUCLEAR_FUEL_REPROCESSING = registerResearch(Technology("nuclear_fuel_reprocessing", setOf(NUCLEAR_POWER, PRODUCTION_SCIENCE_PACK), setOf(automation, logic, chemical, production), 50, 600))
	val LOGISTICS_3 = registerResearch(Technology("logistics_3", setOf(PRODUCTION_SCIENCE_PACK, LUBRICANT), setOf(automation, logic, chemical, production), 300, 300))
	val POWER_ARMOR = registerResearch(Technology("power_armor", setOf(MODULAR_ARMOR, ELECTRIC_ENGINE, ADVANCED_ELECTRONICS_2), setOf(automation, logic, chemical), 200, 600))
	val ROBOTICS = registerResearch(Technology("robotics", setOf(ELECTRIC_ENGINE, BATTERY), setOf(automation, logic, chemical), 75, 600))
	val CONSTRUCTION_ROBOTICS = registerResearch(Technology("construction_robotics", setOf(ROBOTICS), setOf(automation, logic, chemical), 100, 600))
	val LOGISTIC_ROBOTICS = registerResearch(Technology("logistic_robotics", setOf(ROBOTICS), setOf(automation, logic, chemical), 250, 600))
	val WORKER_ROBOTS_SPEED_1 = registerResearch(Technology("worker_robots_speed_1", setOf(ROBOTICS), setOf(automation, logic, chemical), 50, 600))
	val WORKER_ROBOTS_SPEED_2 = registerResearch(Technology("worker_robots_speed_2", setOf(WORKER_ROBOTS_SPEED_1), setOf(automation, logic, chemical), 100, 600))
	val WORKER_ROBOTS_SPEED_3 = registerResearch(Technology("worker_robots_speed_3", setOf(WORKER_ROBOTS_SPEED_2), setOf(automation, logic, chemical, utility), 150, 1200))
	val WORKER_ROBOTS_SPEED_4 = registerResearch(Technology("worker_robots_speed_4", setOf(WORKER_ROBOTS_SPEED_3), setOf(automation, logic, chemical, utility), 250, 1200))
	val WORKER_ROBOTS_SPEED_5 = registerResearch(Technology("worker_robots_speed_5", setOf(WORKER_ROBOTS_SPEED_4), setOf(automation, logic, chemical, production, utility), 500, 1200))
	val WORKER_ROBOTS_STORAGE_1 = registerResearch(Technology("worker_robots_storage_1", setOf(ROBOTICS), setOf(automation, logic, chemical), 200, 600))
	val WORKER_ROBOTS_STORAGE_2 = registerResearch(Technology("worker_robots_storage_2", setOf(WORKER_ROBOTS_STORAGE_1), setOf(automation, logic, chemical, production), 300, 1200))
	val WORKER_ROBOTS_STORAGE_3 = registerResearch(Technology("worker_robots_storage_3", setOf(WORKER_ROBOTS_STORAGE_2), setOf(automation, logic, chemical, production, utility), 450, 1200))
	val ENERGY_SHIELD_MK2_EQUIPMENT = registerResearch(Technology("energy_shield_mk2_equipment", setOf(ENERGY_SHIELD_EQUIPMENT, MILITARY_3, LOW_DENSITY_STRUCTURE, POWER_ARMOR), setOf(automation, logic, chemical, militia), 200, 600))
	val BATTERY_MK2_EQUIPMENT = registerResearch(Technology("battery_mk2_equipment", setOf(BATTERY_EQUIPMENT, LOW_DENSITY_STRUCTURE, POWER_ARMOR), setOf(automation, logic, chemical), 100, 600))
	val PERSONAL_LASER_DEFENSE_EQUIPMENT = registerResearch(Technology("personal_laser_defense_equipment", setOf(LASER_TURRET, MILITARY_3, LOW_DENSITY_STRUCTURE, POWER_ARMOR, SOLAR_PANEL_EQUIPMENT), setOf(automation, logic, chemical, militia), 100, 600))
	val DISCHARGE_DEFENSE_EQUIPMENT = registerResearch(Technology("discharge_defense_equipment", setOf(LASER_TURRET, MILITARY_3, POWER_ARMOR, SOLAR_PANEL_EQUIPMENT), setOf(automation, logic, chemical, militia), 100, 600))
	val PERSONAL_ROBOPORT_EQUIPMENT = registerResearch(Technology("personal_roboport_equipment", setOf(CONSTRUCTION_ROBOTICS, SOLAR_PANEL_EQUIPMENT), setOf(automation, logic, chemical), 50, 600))
	val UTILITY_SCIENCE_PACK = registerResearch(Technology("utility_science_pack", setOf(ROBOTICS, ADVANCED_ELECTRONICS_2, LOW_DENSITY_STRUCTURE), setOf(automation, logic, chemical), 100, 600))
	val MILITARY_4 = registerResearch(Technology("military_4", setOf(MILITARY_3, UTILITY_SCIENCE_PACK, EXPLOSIVES), setOf(automation, logic, chemical, militia, utility), 150, 900))
	val URANIUM_AMMO = registerResearch(Technology("uranium_ammo", setOf(URANIUM_PROCESSING, MILITARY_4, TANK), setOf(automation, logic, chemical, militia, utility), 1000, 900))
	val POWER_ARMOR_MK2 = registerResearch(Technology("power_armor_mk2", setOf(POWER_ARMOR, MILITARY_4, SPEED_MODULE_2, EFFECTIVITY_MODULE_2), setOf(automation, logic, chemical, militia, utility), 400, 600))
	val ROCKET_CONTROL_UNIT = registerResearch(Technology("rocket_control_unit", setOf(UTILITY_SCIENCE_PACK, SPEED_MODULE), setOf(automation, logic, chemical, utility), 300, 900))
	val ROCKET_SILO = registerResearch(Technology("rocket_silo", setOf(CONCRETE, SPEED_MODULE_3, PRODUCTIVITY_MODULE_3, ROCKET_FUEL, ROCKET_CONTROL_UNIT), setOf(automation, logic, chemical, production, utility), 1000, 1200))
	val LOGISTIC_SYSTEM = registerResearch(Technology("logistic_system", setOf(UTILITY_SCIENCE_PACK, LOGISTIC_ROBOTICS), setOf(automation, logic, chemical, utility), 500, 600))
	val FUSION_REACTOR_EQUIPMENT = registerResearch(Technology("fusion_reactor_equipment", setOf(UTILITY_SCIENCE_PACK, POWER_ARMOR, MILITARY_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility), 200, 600))
	val PERSONAL_ROBOPORT_MK2_EQUIPMENT = registerResearch(Technology("personal_roboport_mk2_equipment", setOf(PERSONAL_ROBOPORT_EQUIPMENT, UTILITY_SCIENCE_PACK), setOf(automation, logic, chemical, utility), 250, 600))
	val DESTROYER = registerResearch(Technology("destroyer", setOf(MILITARY_4, DISTRACTOR, SPEED_MODULE), setOf(automation, logic, chemical, militia, utility), 300, 600))
	val ARTILLERY = registerResearch(Technology("artillery", setOf(MILITARY_4, TANK), setOf(automation, logic, chemical, militia, utility), 2000, 600))
	val SPIDERTRON = registerResearch(Technology("spidertron", setOf(MILITARY_4, EXOSKELETON_EQUIPMENT, FUSION_REACTOR_EQUIPMENT, ROCKETRY, ROCKET_CONTROL_UNIT, EFFECTIVITY_MODULE_3), setOf(automation, logic, militia, chemical, production, utility), 2500, 600))
	val FOLLOWER_ROBOT_COUNT_5 = registerResearch(Technology("follower_robot_count_5", setOf(FOLLOWER_ROBOT_COUNT_4, DESTROYER), setOf(automation, logic, chemical, militia, utility), 800, 600))
	val FOLLOWER_ROBOT_COUNT_6 = registerResearch(Technology("follower_robot_count_6", setOf(FOLLOWER_ROBOT_COUNT_5), setOf(automation, logic, chemical, militia, utility), 1000, 600))
	val SPACE_SCIENCE_PACK = registerResearch(Technology("space_science_pack", setOf(ROCKET_SILO, ELECTRIC_ENERGY_ACCUMULATORS, SOLAR_ENERGY), setOf(automation, logic, chemical, production, utility), 2000, 600))
	val ATOMIC_BOMB = registerResearch(Technology("atomic_bomb", setOf(MILITARY_4, KOVAREX_ENRICHMENT_PROCESS, ROCKET_CONTROL_UNIT, ROCKETRY), setOf(automation, logic, chemical, militia, production, utility), 5000, 900))
	val WORKER_ROBOTS_SPEED_6 = registerResearch(Technology("worker_robots_speed_6", setOf(WORKER_ROBOTS_SPEED_5, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, production, utility, space), 0, 1200))
	val MINING_PRODUCTIVITY_4 = registerResearch(Technology("mining_productivity_4", setOf(MINING_PRODUCTIVITY_3, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, production, utility, space), 0, 1200))
	val PHYSICAL_PROJECTILE_DAMAGE_7 = registerResearch(Technology("physical_projectile_damage_7", setOf(PHYSICAL_PROJECTILE_DAMAGE_6, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val STRONGER_EXPLOSIVES_7 = registerResearch(Technology("stronger_explosives_7", setOf(STRONGER_EXPLOSIVES_6, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val REFINED_FLAMMABLES_7 = registerResearch(Technology("refined_flammables_7", setOf(REFINED_FLAMMABLES_6, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val ENERGY_WEAPONS_DAMAGE_7 = registerResearch(Technology("energy_weapons_damage_7", setOf(ENERGY_WEAPONS_DAMAGE_6, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val ARTILLERY_SHELL_RANGE_1 = registerResearch(Technology("artillery_shell_range_1", setOf(ARTILLERY, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val ARTILLERY_SHELL_SPEED_1 = registerResearch(Technology("artillery_shell_speed_1", setOf(ARTILLERY, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, utility, space), 0, 1200))
	val FOLLOWER_ROBOT_COUNT_7 = registerResearch(Technology("follower_robot_count_7", setOf(FOLLOWER_ROBOT_COUNT_6, SPACE_SCIENCE_PACK), setOf(automation, logic, chemical, militia, production, utility, space), 0, 600))

	fun registerResearch(technology: Technology): Technology {
		return Registry.register(INSTANCE, Identifier(FactorioCraft.ModId, technology.identifier.path), technology)
	}
}