package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.`object`.builder.v1.entity.FabricDefaultAttributeRegistry
import net.fabricmc.fabric.api.`object`.builder.v1.entity.FabricEntityTypeBuilder
import net.minecraft.entity.EntityDimensions
import net.minecraft.entity.EntityType
import net.minecraft.entity.SpawnGroup
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry.ENTITY_TYPE
import net.minecraft.util.registry.Registry.register
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.entitiy.*

class EntityRegistry {
	companion object {
		lateinit var TEST_ENTITY_TYPE: EntityType<TestEntity>
		lateinit var LOGISTIC_ROBOT_ENTITY_TYPE: EntityType<LogisticRobotEntity>
		lateinit var LOGISTICS_ROBOT_ENTITY_TYPE: EntityType<LogisticsRobotEntity>
		lateinit var ITEM_MAGNET_LOGISTIC_ROBOT_ENTITY_TYPE: EntityType<ItemMagnetLogisticRobotEntity>
		lateinit var DEFENSE_ROBOT_ENTITY_TYPE: EntityType<DefenseRobotEntity>

		init {
			TEST_ENTITY_TYPE = register(
				ENTITY_TYPE,
				Identifier(FactorioCraft.ModId, "test_entity"),
				FabricEntityTypeBuilder.create(SpawnGroup.MISC, ::TestEntity).dimensions(EntityDimensions.fixed(.5f, .5f)).build()
			)
			LOGISTIC_ROBOT_ENTITY_TYPE = register(
				ENTITY_TYPE,
				Identifier(FactorioCraft.ModId, "test_logistic_robot"),
				FabricEntityTypeBuilder.create<LogisticRobotEntity>(
					SpawnGroup.MISC, ::LogisticRobotEntity
				).dimensions(EntityDimensions.fixed(0.5f, 0.5f)).build()
			)
			LOGISTICS_ROBOT_ENTITY_TYPE = register(
				ENTITY_TYPE,
				Identifier(FactorioCraft.ModId, "logistic_robot"),
				FabricEntityTypeBuilder.create(SpawnGroup.MISC, ::LogisticsRobotEntity).dimensions(EntityDimensions.fixed(0.75f, 0.75f)).build()
			)
			ITEM_MAGNET_LOGISTIC_ROBOT_ENTITY_TYPE = register(
				ENTITY_TYPE,
				Identifier(FactorioCraft.ModId, "item_magnet_logistic_robot"),
				FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ::ItemMagnetLogisticRobotEntity).dimensions(EntityDimensions.fixed(0.75f, 0.75f)).build()
			)
			DEFENSE_ROBOT_ENTITY_TYPE = register(
				ENTITY_TYPE,
				Identifier(FactorioCraft.ModId, "defense_robot"),
				FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ::DefenseRobotEntity).dimensions(EntityDimensions.fixed(0.75f, 0.75f)).build()
			)
		}

		fun register() {
			FabricDefaultAttributeRegistry.register(TEST_ENTITY_TYPE, TestEntity.createMobAttributes())
			FabricDefaultAttributeRegistry.register(LOGISTIC_ROBOT_ENTITY_TYPE, LogisticRobotEntity.createBeeAttributes())
			FabricDefaultAttributeRegistry.register(LOGISTICS_ROBOT_ENTITY_TYPE, LogisticsRobotEntity.createMobAttributes())
			FabricDefaultAttributeRegistry.register(ITEM_MAGNET_LOGISTIC_ROBOT_ENTITY_TYPE, ItemMagnetLogisticRobotEntity.createMobAttributes())
			FabricDefaultAttributeRegistry.register(DEFENSE_ROBOT_ENTITY_TYPE, DefenseRobotEntity.createMobAttributes())
		}
	}
}