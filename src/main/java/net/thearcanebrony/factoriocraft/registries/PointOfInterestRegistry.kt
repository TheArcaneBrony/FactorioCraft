package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.`object`.builder.v1.world.poi.PointOfInterestHelper
import net.minecraft.util.Identifier
import net.minecraft.world.poi.PointOfInterestType
import net.thearcanebrony.factoriocraft.FactorioCraft

class PointOfInterestRegistry {
	companion object{
		lateinit var ROBOPORT: PointOfInterestType
		fun register(){
			ROBOPORT = PointOfInterestHelper.register(Identifier(FactorioCraft.ModId, "roboport"), 350, 1, BlockRegistry.ROBOPORT)
		}
	}
}