package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry
import net.thearcanebrony.factoriocraft.client.FactorioCraftClient
import net.thearcanebrony.factoriocraft.entitiy.LogisticRobotEntity.LogisticRobotEntityRenderer
import net.thearcanebrony.factoriocraft.entitiy.TestEntity
import net.thearcanebrony.factoriocraft.entitiy.models.LogisticsRobotEntityModel
import net.thearcanebrony.factoriocraft.entitiy.renderers.LogisticsRobotEntityRenderer

class EntityRendererRegistry {
	companion object {
		fun registerRenderers() {

			//robot types
			EntityRendererRegistry.register(EntityRegistry.TEST_ENTITY_TYPE, TestEntity::TestEntityRenderer)
			EntityRendererRegistry.register(EntityRegistry.LOGISTIC_ROBOT_ENTITY_TYPE, ::LogisticRobotEntityRenderer)
			EntityRendererRegistry.register(EntityRegistry.LOGISTICS_ROBOT_ENTITY_TYPE, ::LogisticsRobotEntityRenderer)
			EntityRendererRegistry.register(EntityRegistry.ITEM_MAGNET_LOGISTIC_ROBOT_ENTITY_TYPE, ::LogisticsRobotEntityRenderer)
			EntityRendererRegistry.register(EntityRegistry.DEFENSE_ROBOT_ENTITY_TYPE, ::LogisticsRobotEntityRenderer)
			EntityModelLayerRegistry.registerModelLayer(FactorioCraftClient.MODEL_CUBE_LAYER) { LogisticsRobotEntityModel.getTexturedModelData() }
		}

	}
}