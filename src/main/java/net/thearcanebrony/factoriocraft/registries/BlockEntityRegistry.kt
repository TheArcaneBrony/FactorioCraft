package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.Block
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.util.registry.Registry
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.block.entity.*

class BlockEntityRegistry {
	companion object {
		lateinit var ROBOPORT_BLOCK_ENTITY: BlockEntityType<*>
		lateinit var REQUESTER_CHEST_BLOCK_ENTITY: BlockEntityType<*>
		lateinit var PASSIVE_PROVIDER_CHEST_BLOCK_ENTITY: BlockEntityType<*>
		lateinit var ACTIVE_PROVIDER_CHEST_BLOCK_ENTITY: BlockEntityType<*>
		lateinit var STORAGE_CHEST_BLOCK_ENTITY: BlockEntityType<*>
		lateinit var RESEARCH_LAB_BLOCK_ENTITY: BlockEntityType<*>

		fun register() {
			ROBOPORT_BLOCK_ENTITY = registerBlockEntity<RoboportBlockEntity>(BlockRegistry.ROBOPORT, "roboport_block_entity") { pos: BlockPos?, state: BlockState? -> RoboportBlockEntity(pos, state) }
			REQUESTER_CHEST_BLOCK_ENTITY = registerBlockEntity<RequesterChestBlockEntity>(BlockRegistry.REQUESTER_CHEST, "requester_chest_block_entity") { pos: BlockPos?, state: BlockState? -> RequesterChestBlockEntity(pos, state) }
			PASSIVE_PROVIDER_CHEST_BLOCK_ENTITY = registerBlockEntity<PassiveProviderChestBlockEntity>(BlockRegistry.PASSIVE_PROVIDER_CHEST, "passive_provider_chest_block_entity") { pos: BlockPos?, state: BlockState? -> PassiveProviderChestBlockEntity(pos, state) }
			ACTIVE_PROVIDER_CHEST_BLOCK_ENTITY = registerBlockEntity<ActiveProviderChestBlockEntity>(BlockRegistry.ACTIVE_PROVIDER_CHEST, "active_provider_chest_block_entity") { pos: BlockPos?, state: BlockState? -> ActiveProviderChestBlockEntity(pos, state) }
			STORAGE_CHEST_BLOCK_ENTITY = registerBlockEntity<StorageChestBlockEntity>(BlockRegistry.STORAGE_CHEST, "storage_chest_block_entity") { pos: BlockPos?, state: BlockState? -> StorageChestBlockEntity(pos, state) }
			RESEARCH_LAB_BLOCK_ENTITY = registerBlockEntity<ResearchLabBlockEntity>(BlockRegistry.RESEARCH_LAB, "research_lab_block_entity") { pos: BlockPos?, state: BlockState? -> ResearchLabBlockEntity(pos, state) }
		}

		private fun <T : BlockEntity> registerBlockEntity(block: Block, name: String, entity: FabricBlockEntityTypeBuilder.Factory<T>): BlockEntityType<*> {
			return Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(FactorioCraft.ModId, name), FabricBlockEntityTypeBuilder.create(entity, block).build(null))
		}
	}
}