package net.thearcanebrony.factoriocraft.registries

import net.minecraft.block.Block
import net.minecraft.tag.TagKey
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.thearcanebrony.factoriocraft.FactorioCraft

class BlockTagRegistry {
	companion object {
		var ROBOPORTS: TagKey<Block> = TagKey.of(Registry.BLOCK_KEY, Identifier(FactorioCraft.ModId, "roboports"))

	}
}