package net.thearcanebrony.factoriocraft.registries

import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.SpawnEggItem
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.item.LogisticRobotItem
import net.thearcanebrony.factoriocraft.item.SciencePackItem
import net.thearcanebrony.factoriocraft.item.SciencePackType

class ItemRegistry {
	companion object {

		// - spawn eggs
		lateinit var LOGISTIC_ROBOT_SPAWN_EGG: Item

		// - logistic robots
		lateinit var LOGISTIC_ROBOT_ITEM: Item

		// - science pack items

		lateinit var AUTOMATION_SCIENCE_PACK_ITEM: Item
		lateinit var CHEMICAL_SCIENCE_PACK_ITEM: Item
		lateinit var LOGISTIC_SCIENCE_PACK_ITEM: Item
		lateinit var MILITARY_SCIENCE_PACK_ITEM: Item
		lateinit var PRODUCTION_SCIENCE_PACK_ITEM: Item
		lateinit var SPACE_SCIENCE_PACK_ITEM: Item
		lateinit var UTILITY_SCIENCE_PACK_ITEM: Item

		fun register() {

			//items
			LOGISTIC_ROBOT_SPAWN_EGG = registerItem(SpawnEggItem(EntityRegistry.LOGISTICS_ROBOT_ENTITY_TYPE, 0xFF00FF, 0x00FF00, Item.Settings().group(ItemGroup.MISC)), "logistic_robot_spawn_egg")
			LOGISTIC_ROBOT_ITEM = registerItem(LogisticRobotItem(FabricItemSettings().group(ItemGroup.MISC)), "logistic_robot")
			AUTOMATION_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Automation), "automation_science_pack")
			CHEMICAL_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Chemical), "chemical_science_pack")
			LOGISTIC_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Logistic), "logistic_science_pack")
			MILITARY_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Military), "military_science_pack")
			PRODUCTION_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Production), "production_science_pack")
			SPACE_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Space), "space_science_pack")
			UTILITY_SCIENCE_PACK_ITEM = registerItem(SciencePackItem(SciencePackType.Utility), "utility_science_pack")
		}

		fun registerItem(item: Item, name: String): Item =
			Registry.register(Registry.ITEM, Identifier(FactorioCraft.ModId, name), item)
	}
}