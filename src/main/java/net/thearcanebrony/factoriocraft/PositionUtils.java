package net.thearcanebrony.factoriocraft;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.block.entity.RoboportBlockEntity;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class PositionUtils {
    private static Random random = new Random();
    public static BlockPos searchRoboport(BaseRobotEntity entity, int radius) {
        return searchRoboport(entity.getBlockPos(), entity, radius);
    }
    public static BlockPos searchBlock(Entity entity, Block blocktype, int radius) {
        return searchBlock(entity.getBlockPos(), entity.getWorld(), blocktype, radius);
    }
    public static BlockPos searchRoboport(BlockPos blockPos, BaseRobotEntity entity, int radius) {
        ArrayList<RoboportBlockEntity> roboports = new ArrayList<>();
        for (var rbp : LogisticNetwork.roboports)
            if(rbp.getPos().isWithinDistance(blockPos, radius))
                roboports.add(rbp);
        if(roboports.size() == 0) return null;
        roboports.sort(Comparator.comparingDouble(port -> port.getPos().getSquaredDistance(blockPos)));
        return roboports.get(0).getPos();
//        return searchBlock(blockPos, world, FactorioCraft.ROBOPORT, radius);
    }
    public static BlockPos searchBlock(BlockPos bp, World world, Block blocktype, int radius) {
        BlockPos spos = null;
        long time = System.currentTimeMillis();
        int i = 0;
        int fx= bp.getX()-radius/2;
        int ex= bp.getX()+radius/2;
        int fy= Math.max(bp.getY()-radius/2, -32);
        int ey= Math.min(bp.getY()+radius/2, 320);
        int fz= bp.getZ()-radius/2;
        int ez= bp.getZ()+radius/2;
        for (int ty = bp.getY()-2; ty < bp.getY()+2; ty++) {
            for (int tx = fx; tx < ex; tx++) {
                for (int tz = fz; tz < ez; tz++) {
                    i++;
                    if(world.getBlockState(new BlockPos(tx,ty,tz)).isOf(blocktype)) {
                        System.out.printf("Found block after %s iterations (%s ms)\n", i, System.currentTimeMillis()-time);
                        return new BlockPos(tx,ty,tz);
                    }
                }
            }
        }
        for (int ty = fy; ty < ey; ty++) {
            for (int tx = fx; tx < ex; tx++) {
                for (int tz = fz; tz < ez; tz++) {
                    i++;
                    if(world.getBlockState(new BlockPos(tx,ty,tz)).isOf(blocktype)) {
                        System.out.printf("Found block after %s iterations (%s ms)\n", i, System.currentTimeMillis()-time);
                        return new BlockPos(tx,ty,tz);
                    }
                }
            }
        }
        return bp;
//        do {
//            spos = bp.add(rnd.nextInt(radius * 2) - radius, bp.getY(), rnd.nextInt(radius * 2) - radius);
//            i++;
//            if(i > 20000) {
////                System.out.printf("No block after %s iterations\n", i);
//                return bp;
//            }
//        } while (spos == null || !world.getBlockState(spos).isOf(blocktype));
//        System.out.printf("Found block after %s iterations\n", i);
//        return spos;
//        int rad = 0;
//        do {
//            for (int i = 0; i < rad; i++) {
//                int y = -64;
//                do {
//                    BlockPos ntgt = bp.add(rnd.nextInt(rad * 2) - rad, y, rnd.nextInt(rad * 2) - rad);
//                    if (world.getBlockState(ntgt).isOf(blocktype)) {
//                        return ntgt;
//                    }
//                } while (!world.isOutOfHeightLimit(++y));
//            }
//        } while (++rad <= radius);
//        return bp.add(rnd.nextInt(20) - 10, 0, rnd.nextInt(20) - 10);
    }
}
