package net.thearcanebrony.factoriocraft;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.command.CommandBuildContext;
import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.IdentifierArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.thearcanebrony.factoriocraft.client.gui.screen.ResearchScreen;
import net.thearcanebrony.factoriocraft.client.gui.swing.FactorioCraftDebugPanel;
import net.thearcanebrony.factoriocraft.entitiy.LogisticsRobotEntity;
import net.thearcanebrony.factoriocraft.registries.*;
import net.thearcanebrony.factoriocraft.staticdata.FactorioStateHolder;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;
import org.quiltmc.qsl.command.api.CommandRegistrationCallback;
import org.quiltmc.qsl.lifecycle.api.event.ServerLifecycleEvents;
import org.quiltmc.qsl.lifecycle.api.event.ServerTickEvents;
import org.quiltmc.qsl.networking.api.PacketByteBufs;
import software.bernie.geckolib3.GeckoLib;

import java.text.DecimalFormat;

import java.util.Objects;
public class FactorioCraft implements ModInitializer {
    // Misc
    public static final String ModId = "factoriocraft";
    public static final Logger LOGGER = LogManager.getLogger("FactorioCraft");
    public static final Identifier DEBUG_PATHFINDING_CHANNEL = new Identifier(ModId, "debug_pathfinding");
    //Screens+Handlers
    //public static final ScreenHandlerType<ResearchScreen.ResearchScreenHandler> RESEARCH_SCREEN_HANDLER_TYPE = ScreenHandlerRegistry.registerSimple(new Identifier(ModId, "research_screen"), ResearchScreen::new)
    public static final FactorioCraftDebugPanel[] debugPanel = new FactorioCraftDebugPanel[1];
    private static DecimalFormat df = new DecimalFormat("#.##");

    private static int executeFoo(CommandContext<ServerCommandSource> context) {
        System.out.println("Called foo with no arguments");
        var s = context.getSource();
        var w = s.getWorld();
        var count = 100;
        try {
            count = IntegerArgumentType.getInteger(context, "count");
        } catch (Exception ignored) {
        }
        LOGGER.info(s.getPosition());
        for (int i = 0; i < count; i++) {
            Entity myEnt;
            w.spawnEntity(myEnt = new LogisticsRobotEntity(EntityRegistry.Companion.getLOGISTICS_ROBOT_ENTITY_TYPE(), w));
            myEnt.teleport(s.getPosition().getX(), s.getPosition().getY(), s.getPosition().getZ());
        }

        return 1;
    }
    //public static final ScreenHandlerType<InventoryScreen6x9Handler> INVENTORY_SCREEN_6X9 = ScreenHandlerRegistry.registerSimple(new Identifier(ModId, "inventory_screen_6x9"), InventoryScreen6x9Handler::new);

    private static void registerCommand(CommandDispatcher<ServerCommandSource> dispatcher, CommandBuildContext buildContext, CommandManager.RegistrationEnvironment environment) {
        dispatcher.register(CommandManager.literal(
                "foo"
        ).then(
                CommandManager.argument(
                        "count",
                        IntegerArgumentType.integer()
                ).executes(FactorioCraft::executeFoo)
        ).executes(FactorioCraft::executeFoo));

        dispatcher.register(CommandManager.literal(
                "research"
        ).then(
                CommandManager.literal(
                        "grant"
                ).then(
                        CommandManager.argument(
                                "research",
                                IdentifierArgumentType.identifier()
                        ).suggests(
                                (context, builder) -> CommandSource.suggestIdentifiers(Technologies.getINSTANCE().getIds(), builder)
                        ).executes(FactorioCraft::executeGrantTechnology)
                )
        ));
    }

    private static int executeGrantTechnology(CommandContext<ServerCommandSource> context) {
        var research = Objects.requireNonNull(Technologies.getINSTANCE().get(IdentifierArgumentType.getIdentifier(context, "research")));
        int unlocked = ((FactorioStateHolder) context.getSource().getServer()).getFactorioState().researchState.unlock(research);
        switch (unlocked) {
            case 0 -> context.getSource().sendFeedback(Text.translatable("command.factoriocraft.unlock.fail", Text.translatable(research.getTransKey()).formatted(Formatting.BOLD)), false);
            case 1 -> context.getSource().sendFeedback(Text.translatable("command.factoriocraft.unlock.success", Text.translatable(research.getTransKey()).formatted(Formatting.BOLD)), false);
            default -> context.getSource().sendFeedback(Text.translatable("command.factoriocraft.unlock.multi", Text.translatable(research.getTransKey()).formatted(Formatting.BOLD), unlocked), false);
        }
        return unlocked;
    }

    private static void onEndTick(MinecraftServer server) {
//        var list = new ArrayList<BaseRobotEntity>(server.getOverworld().getEntitiesByClass(
//                LogisticsRobotEntity.class,
//                Box.of(new Vec3d(0, 0, 0), 30_000_000, 1000, 30_000_000),
//                __ -> true
//        ));
//        for (var entity : list) {
//            if (entity == null || entity.getServer() == null || entity.isDead() || entity.isRemoved() || !entity.getServer().equals(server)) {
//                LogisticNetwork.removeRobot(entity);
//            }
//        }
        if (LogisticNetwork.logisticRobots.size() > 0) {
            LogisticsRobotEntity logisticsRobot = LogisticNetwork.logisticRobots.stream().findAny().get();
            if (
                    logisticsRobot.getGtrg() != null &&
                    logisticsRobot.getGtrg().getPathFinder() != null &&
                    logisticsRobot.getGtrg().getPathFinder().nodes.size() > 0
            ) {
                PacketByteBuf packet = PacketByteBufs.create();
                logisticsRobot.getGtrg().getPathFinder().writeData(packet);
                server.getPlayerManager().sendToAll(new CustomPayloadS2CPacket(DEBUG_PATHFINDING_CHANNEL, packet));
            }
        }
        if (debugPanel[0] != null) debugPanel[0].updateDebugData();

        ((FactorioStateHolder) server).getFactorioState().tick();
        if (server.getOverworld().getTime() % 20 == 0) ((FactorioStateHolder) server).saveState();
    }

    private static void onServerClose(MinecraftServer minecraftServer) {
        LogisticNetwork.clear();
    }

    private static void onServerStart(MinecraftServer minecraftServer) {
        ((FactorioStateHolder) minecraftServer).loadState();
    }

    @Override
    public void onInitialize(ModContainer mod) {

        // Innitialize GeckLib
//        GeckoLibMod.DISABLE_IN_DEV = true;
        GeckoLib.initialize();
        //register blocks
        BlockRegistry.Companion.register();

        //register TEs
        BlockEntityRegistry.Companion.register();

        //entities
        EntityRegistry.Companion.register();

        //items
        ItemRegistry.Companion.register();

        PointOfInterestRegistry.Companion.register();

        BlockTagRegistry.Companion.getROBOPORTS();

        CommandRegistrationCallback.EVENT.register(FactorioCraft::registerCommand);

        //debug data syncing
        ServerTickEvents.END.register(FactorioCraft::onEndTick);
        ServerLifecycleEvents.STOPPED.register(FactorioCraft::onServerClose);
        ServerLifecycleEvents.READY.register(FactorioCraft::onServerStart);

        //misc
        DataTrackerRegistry.Companion.register();

        HandledScreens.register(ResearchScreen.ResearchScreenHandler.Companion.getResearchScreenHandlerType(), ResearchScreen::new);


//        System.exit(0);
//        var fcg = new FactoriocraftGui();

    }
}
