package net.thearcanebrony.factoriocraft.research

import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.thearcanebrony.factoriocraft.FactorioCraft
import java.lang.Integer.max

class Technology(val identifier: Identifier = Identifier(FactorioCraft.ModId, "root"), val parents: Set<Technology> = setOf(), val packs: Set<ItemStack> = setOf(), val researchCount: Int = 0, val researchTime: Int = 0) {
	constructor(name: String, parents: Set<Technology>, packs: Set<ItemStack>, researchCount: Int = 1, researchTime: Int = 0) : this(Identifier(FactorioCraft.ModId, name), parents, packs, researchCount, researchTime)

	override fun toString(): String = "($identifier: ${parents.map { it.identifier }}; $packs; count: $researchCount, time: $researchTime)"
	fun getTransKey(): String = "technology.${identifier.namespace}.${identifier.path}"
	fun getProgressLevel(): Int {
		return if(parents.isEmpty()) 0 else parents.reduce { acc, research ->
			return max(acc.getProgressLevel(), research.getProgressLevel())
		}.getProgressLevel() + 1
	}
}