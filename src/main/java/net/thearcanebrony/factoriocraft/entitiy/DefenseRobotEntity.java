package net.thearcanebrony.factoriocraft.entitiy;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.MoveToTargetPosGoal;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.goals.robot.GoToRoboportGoal;

import java.util.Random;

public class DefenseRobotEntity extends BaseRobotEntity {
    private static final Random rnd = new Random();
    private static long next_id = 0;
    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(2, ItemStack.EMPTY);
    private long id = 0;
    private BlockPos target = new BlockPos(0, 0, 0);
    private long ticks = 0;
    private int randomTickOffset = 20;
    private MoveToTargetPosGoal mttpg;
    private GoToRoboportGoal gtrg = null;

    public DefenseRobotEntity(EntityType<? extends BaseRobotEntity> entityType, World world) {
        super(entityType, world);
        id = next_id;
        if (!world.isClient) next_id++;
        randomTickOffset = rnd.nextInt(15);
        items.set(0, new ItemStack(Items.DIAMOND, 1));
    }

    public BlockPos searchItem() {
        var ls = world.getOtherEntities(this, new Box(getBlockPos().add(-50, -10, -50), getBlockPos().add(50, 10, 50)), entity -> entity instanceof HostileEntity);
        if (ls.size() == 0) return getBlockPos();
        if (ls.size() == 1) return ls.get(0).getBlockPos();
        ls.sort((x, y) -> (int) x.distanceTo(this) < y.distanceTo(this) ? 1 : 0);
        return ls.get(0).getBlockPos();
    }

    public HostileEntity searchNearestItem() {
        var ls = world.getOtherEntities(this, new Box(getBlockPos().add(-2, -2, -2), getBlockPos().add(2, 2, 2)), entity -> entity instanceof HostileEntity);
        if (ls.size() == 0) return null;
        if (ls.size() == 1) return (HostileEntity) ls.get(0);
        ls.sort((x, y) -> (int) (y.distanceTo(this) - x.distanceTo(this)));
        return (HostileEntity) ls.get(0);
    }

    @Override
    protected void tickCramming() {
        super.tickCramming();
    }

    @Override
    protected void initGoals() {
        super.initGoals();
//        goalSelector.add(1, new SwimGoal(this) {
//            @Override
//            public boolean shouldContinue() {
//                return world.getBlockState(getBlockPos()).isOf(Blocks.WATER);
//            }
//        });
        goalSelector.add(1, gtrg = new GoToRoboportGoal(this, 100));

//        goalSelector.add(20, new WanderAroundGoal(this, 0.5, 25));
        System.out.println("Added goals");
    }

    @Override
    protected void updateGoalControls() {
        super.updateGoalControls();
    }

    @Override
    public void tick() {
        super.tick();
        if (!world.isClient) {
            if(gtrg != null && gtrg.target != null) setCustomName(Text.literal(String.format("DR %s: %s (HP: %.2f)", id, gtrg.target, getHealth())));
            else setCustomName(Text.literal(String.format("DR %s: no target (HP: %.2f)", id, getHealth())));
        }

//        if (ticks % 20 == randomTickOffset) {
        target = searchItem();
        if (target.isWithinDistance(getBlockPos(), 2)) {
            var e = searchNearestItem();
            if (e != null) {
//                        e.animateDamage();
//                        e.setHealth(e.getHealth() - 2);
                e.kill();
            }
        }
//        }
        if(gtrg != null) gtrg.target = target.add(0, 1, 0);
        else goalSelector.add(1, gtrg = new GoToRoboportGoal(this, 100));


        if (!world.isClient) {
            ticks++;
//            setCustomName(Text.literal(String.format("IM %s: %s (HP: %.2f) %s", id, gtrg.target, getHealth(), items.get(1))));

            if(gtrg != null && gtrg.target != null) setCustomName(Text.literal(String.format("DR %s: %s (HP: %.2f)", id, gtrg.target, getHealth())));
            else setCustomName(Text.literal(String.format("DR %s: no target (HP: %.2f)", id, getHealth())));
        }
    }


    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }

    //    @Nullable
//    @Override
//    public Text getCustomName() {
////        return Text.literal(String.format("Robot %s: %s/%s/%s", getSavedEntityId(), target.getX(), target.getY(), target.getZ()));
//        return super.getCustomName();
//    }
}
