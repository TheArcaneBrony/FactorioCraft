package net.thearcanebrony.factoriocraft.entitiy.pathfinding;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class RobotPathNode {
    public World world;
    public BlockPos pos;
    public Vec3d accuratePos;
//    private static int ticks = 0;
    public RobotPathNode (World world, BlockPos pos){
        this.world = world;
        this.pos = pos;
        accuratePos = Vec3d.of(pos);
    }
    public RobotPathNode (World world, Vec3d pos){
        this.world = world;
        this.accuratePos = pos;
        this.pos = new BlockPos(accuratePos);
    }
    public void tick(int ticks){
//        world.addImportantParticle(ParticleTypes.ENCHANT, true, pos.getX(), pos.getY(), pos.getZ(), 0,0,0);
//        if((ticks & 15) == 0)
//        ((ServerWorld) world).spawnParticles(ParticleTypes.BUBBLE_COLUMN_UP, (double)pos.getX()+0.5d, (double)pos.getY()+0.5d, (double) pos.getZ()+0.5d, 1,0d,0d, 0d, 0d);
//        System.out.printf("Point at %s ticked (client: %s)!\n", pos, world.isClient);
        pos = new BlockPos(accuratePos);
    }

    @Override
    public String toString() {
        return "RobotPathNode{" +
                "pos=" + pos +
                '}';
    }
}
