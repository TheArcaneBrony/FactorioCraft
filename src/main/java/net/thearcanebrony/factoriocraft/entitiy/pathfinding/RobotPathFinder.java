package net.thearcanebrony.factoriocraft.entitiy.pathfinding;

import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.entity.ai.pathing.NavigationType;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.packet.s2c.play.CustomPayloadS2CPacket;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.Heightmap;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;

import java.util.*;

import static net.thearcanebrony.factoriocraft.FactorioCraft.DEBUG_PATHFINDING_CHANNEL;

public class RobotPathFinder {
    private static final BlockPos[] OFFSETS;

    static {
        var list = new ArrayList<BlockPos>();
//        for (Direction d : Direction.values()){
//            list.add(new BlockPos(d.getVector()));
//        }
//        for(BlockPos pos : BlockPos.iterate(new BlockPos(-1, -1, -1), new BlockPos(1, 1, 1))){
//            if(pos.getX() == 0 && pos.getY() == 0 && pos.getZ() == 0) continue;;
//            list.add(pos.mutableCopy());
//        }
        list.add(new BlockPos(1, 0, 0));
        list.add(new BlockPos(0, 1, 0));
        list.add(new BlockPos(0, 0, 1));
        list.add(new BlockPos(-1, 0, 0));
        list.add(new BlockPos(0, -1, 0));
        list.add(new BlockPos(0, 0, -1));
        OFFSETS = list.toArray(BlockPos[]::new);
    }

    public ArrayList<RobotPathNode> nodes = new ArrayList<>();
    public BaseRobotEntity entity;
    private int ticks = 0;
    private BlockPos target = null;
    private World world = null;

    public RobotPathFinder(BaseRobotEntity entity) {
        this.entity = entity;
    }

    public void tick() {
        ticks++;
        for (var node : nodes) {
            node.tick(ticks);

        }
    }

    public void pathToTarget(BlockPos current, BlockPos target) {
        this.target = target;
        this.world = entity.world;
        nodes.clear();
//        var lastNode = new RobotPathNode(world, current);
//        LOGGER.info("Current position: {}, to: {}, shortest distance: {}", current.toShortString(), target.toShortString(), current.getManhattanDistance(target));
        Stack<Pair<BlockPos, Integer>> pathStack = new Stack<>();
        pathStack.push(new Pair<>(current, -1));
        HashSet<BlockPos> deadEnds = new HashSet<>();
        HashSet<BlockPos> visited = new HashSet<>();
        final BlockPos.Mutable currentBlockPos = current.mutableCopy();
        int continuationIndex = 0;
        boolean noPath = false;
        finished:
        while (!noPath) {
            nextStep:
            {

                if (!pathStack.empty()) {
                    currentBlockPos.set(pathStack.peek().getLeft());
                }
                Arrays.sort(OFFSETS, Comparator.comparingDouble(a -> currentBlockPos.toImmutable().mutableCopy().add(a).getManhattanDistance(target)/* + abs(a.getY()) * 0*/));
                for (int i = continuationIndex; i < OFFSETS.length; i++) {
                    if (!pathStack.empty()) {
                        currentBlockPos.set(pathStack.peek().getLeft());
//                        LOGGER.info("Current: {}, direction: {}", currentBlockPos.toShortString(), OFFSETS[i].toShortString());
                    }
                    currentBlockPos.move(OFFSETS[i]);
                    if (pathStack.size() > 1000) {
//                        LOGGER.info("no path after 40 steps");
                        for (var p : pathStack) {
                            nodes.add(new RobotPathNode(world, p.getLeft()));
                        }
                        PacketByteBuf packet = PacketByteBufs.create();
                        writeData(packet);
                        Objects.requireNonNull(entity.getServer()).getPlayerManager().sendToAll(new CustomPayloadS2CPacket(DEBUG_PATHFINDING_CHANNEL, packet));
                        nodes.clear();
                        return;
                    }
                    if (
                            deadEnds.contains(currentBlockPos) ||
                                    visited.contains(currentBlockPos) || (
                                    !pathStack.empty() &&
                                            pathStack.size() > 1 &&
                                            currentBlockPos.getX() == pathStack.get(pathStack.size() - 2).getLeft().getX() &&
                                            currentBlockPos.getY() == pathStack.get(pathStack.size() - 2).getLeft().getY()
                            )
                    ) {
//                        LOGGER.info("backtrack");
                        if (i == OFFSETS.length - 1) {
                            if (pathStack.size() < 2) {
                                noPath = true;
                                break;
                            }
                            deadEnds.add(currentBlockPos.toImmutable());
                            deadEnds.add(currentBlockPos.subtract(OFFSETS[i]).toImmutable());
                            visited.remove(currentBlockPos);
                            var p = pathStack.pop();
                            currentBlockPos.set(p.getLeft());

                            continuationIndex = p.getRight();
                            break nextStep;
                        }
                        continue;
                    }
                    if (currentBlockPos.equals(target)) {
//                        LOGGER.info("reached target");
                        BlockPos immutable = currentBlockPos.toImmutable();
                        pathStack.push(new Pair<>(currentBlockPos.subtract(OFFSETS[i]).toImmutable(), 0));
                        pathStack.push(new Pair<>(immutable, 0));
//                        for(var p : pathStack){
//                            nodes.add(new RobotPathNode(world, p.getLeft()));
//                        }
//                        PacketByteBuf packet = PacketByteBufs.create();
//                        writeData(packet);
//                        Objects.requireNonNull(entity.getServer()).getPlayerManager().sendToAll(new CustomPayloadS2CPacket(DEBUG_PATHFINDING_CHANNEL, packet));
//                        nodes.clear();
                        break finished;
                    }
                    if (validatePosition(currentBlockPos) && canMoveTo(currentBlockPos, OFFSETS[i])) {
//                        LOGGER.info("valid position, propagating");
                        visited.add(currentBlockPos);
                        pathStack.push(new Pair<>(currentBlockPos.toImmutable(), continuationIndex));
                        continuationIndex = 0;
                        break nextStep;
                    } else {
                        deadEnds.add(currentBlockPos.toImmutable());
//                        LOGGER.info("dead-end");
                    }
//                LOGGER.info(OFFSETS[i]);
                }
            }
//            noPath = true;
        }
//        nodes.add(new RobotPathNode(world, current));
        for (var p : pathStack) {
            nodes.add(new RobotPathNode(world, p.getLeft()));
//            LOGGER.info(""p.getLeft());
        }

//        entity.getMovementDirection().
//        System.out.printf("dist: %s, angle: %s, nodes: %s\n", dist, angle, nodes.size());
    }

    private boolean canMoveTo(BlockPos to, BlockPos from) {
        return true;
    }

    private boolean validatePosition(Vec3d pos) {
        return validatePosition(new BlockPos(pos));
    }

    private boolean validatePosition(BlockPos pos) {
        if (target == null) return false;

        if (Vec3d.of(pos).distanceTo(Vec3d.of(target)) > 200) return false;
//        return true;
        Block[] blocksToAvoid = {Blocks.FIRE, Blocks.WATER, Blocks.LAVA, Blocks.CAMPFIRE};
        for (Block b : blocksToAvoid) {
            if (world.getBlockState(pos.add(0, 0, 0)).isOf(b)) return false;
            if (world.getBlockState(pos.add(0, -1, 0)).isOf(b)) return false;
        }

        if (world.getBlockState(pos.add(0, -2, 0)).isOf(Blocks.CAMPFIRE)) return false;
        if (world.getBlockState(pos.add(0, -1, 0)).isOf(Blocks.AIR))
            if (world.getTopY(Heightmap.Type.WORLD_SURFACE, pos.getX(), pos.getZ()) < world.getBottomY()) return false;

        //if (world.getBlockState(pos.add(0, -1, 0)).isOf(Blocks.AIR)) return false;
        if(!world.getFluidState(pos.add(0, -1, 0)).isEmpty()) return false;
        System.out.printf("Found valid target position: pos: %s, target: %s, dist: %s\n", pos, target, pos.getSquaredDistance(target));
        return true;
    }

    public void writeData(PacketByteBuf buffer) {
        buffer.writeInt(entity.getRobotId());
        buffer.writeInt(nodes.size());
        for (var node : nodes) {
            buffer.writeLong(node.pos.asLong());
        }
    }
}
