package net.thearcanebrony.factoriocraft.entitiy;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.ai.goal.MoveToTargetPosGoal;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.PositionUtils;
import net.thearcanebrony.factoriocraft.goals.robot.GoToRoboportGoal;

import java.util.Random;

public class ItemMagnetLogisticRobotEntity extends BaseRobotEntity {
    private static final Random rnd = new Random();
    private static long next_id = 0;
    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(2, ItemStack.EMPTY);
    private long id = 0;
    private BlockPos target = new BlockPos(0, 0, 0);
    private long ticks = 0;
    private int randomTickOffset = 20;
    private MoveToTargetPosGoal mttpg;

    public ItemMagnetLogisticRobotEntity(EntityType<? extends BaseRobotEntity> entityType, World world) {
        super(entityType, world);
        id = next_id;
        if (!world.isClient) next_id++;
        randomTickOffset = rnd.nextInt(15);
        //items.set(0, new ItemStack(Items.DIAMOND, 1));
    }

    public BlockPos searchItem() {
        var ls = world.getOtherEntities(this, new Box(getBlockPos().add(-10, -10, -10), getBlockPos().add(10, 10, 10)), entity -> entity instanceof ItemEntity);
        if (ls.size() == 0) return getBlockPos();
        if (ls.size() == 1) return ls.get(0).getBlockPos();
        ls.sort((x, y) -> (int) x.distanceTo(this) < y.distanceTo(this) ? 1 : 0);
        return ls.get(0).getBlockPos();
    }
    public ItemEntity searchNearestItem() {
        var ls = world.getOtherEntities(this, new Box(getBlockPos().add(-1, -3, -1), getBlockPos().add(1, 1, 1)), entity -> entity instanceof ItemEntity);
        if (ls.size() == 0) return null;
        if (ls.size() == 1) return (ItemEntity) ls.get(0);
        ls.sort((x, y) -> (int) x.distanceTo(this) < y.distanceTo(this) ? 1 : 0);
        return (ItemEntity) ls.get(0);
    }

    @Override
    protected void tickCramming() {
        super.tickCramming();
    }
    private GoToRoboportGoal gtrg;
    @Override
    protected void initGoals() {
        super.initGoals();
//        goalSelector.add(1, new SwimGoal(this) {
//            @Override
//            public boolean shouldContinue() {
//                return world.getBlockState(getBlockPos()).isOf(Blocks.WATER);
//            }
//        });
        goalSelector.add(2, gtrg = new GoToRoboportGoal(this, 100));
        // goalSelector.add(20, new WanderAroundGoal(this, 0.5, 25));
    }

    @Override
    protected void updateGoalControls() {
        super.updateGoalControls();
        if (ticks % 40 == randomTickOffset) {
            if (items.get(1).isEmpty()) {
                target = searchItem();
                if(target.isWithinDistance(getBlockPos(), 2)) {
                    var ie = searchNearestItem();
                    if(ie != null) {
                        items.set(1, ie.getStack());
                        ie.setDespawnImmediately();
                    }
                }
            }
            else target = PositionUtils.searchRoboport(this, 100);
//            gtrg.target = target.add(0,1,0);
        }

        if (!world.isClient){
            ticks++;
            setCustomName(Text.literal(String.format("IM %s: %s (HP: %.2f) %s", id, gtrg.target, getHealth(), items.get(1))));
        }
    }

    @Override
    public void tick() {
        super.tick();
        if(gtrg != null) { setCustomName(Text.literal(String.format("IM %s: %s (HP: %.2f) %s", id, gtrg.target, getHealth(), items.get(1)))); }
    }


    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }

    //    @Nullable
//    @Override
//    public Text getCustomName() {
////        return Text.literal(String.format("Robot %s: %s/%s/%s", getSavedEntityId(), target.getX(), target.getY(), target.getZ()));
//        return super.getCustomName();
//    }
}
