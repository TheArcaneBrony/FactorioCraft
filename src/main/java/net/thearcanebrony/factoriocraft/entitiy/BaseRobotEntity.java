package net.thearcanebrony.factoriocraft.entitiy;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.GenericInventory;
import net.thearcanebrony.factoriocraft.registries.DataTrackerRegistry;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;

public class BaseRobotEntity extends PathAwareEntity implements GenericInventory {
    private static int next_id = 0;
    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(2, ItemStack.EMPTY);
    protected static final TrackedData<Integer> ID = DataTracker.registerData(BaseRobotEntity.class, TrackedDataHandlerRegistry.INTEGER);

    protected static final TrackedData<BlockPos> TARGET = DataTracker.registerData(BaseRobotEntity.class, DataTrackerRegistry.Companion.getNULLABLE_BLOCK_POS());

    public BaseRobotEntity(EntityType<? extends BaseRobotEntity> entityType, World world) {
        super(entityType, world);
        if (!world.isClient) next_id++;
        setNoGravity(true);
        if (!world.isClient) LogisticNetwork.addRobot(this);
//        setNoGravity(true);
    }

    public String getRobotType() {
        return "Generic";
    }

    @Override
    protected void initDataTracker() {
        super.initDataTracker();

        this.dataTracker.startTracking(ID, next_id);
        this.dataTracker.startTracking(TARGET, null);
    }

    @Override
    public boolean cannotDespawn() {
        return true;
    }

    @Override
    protected void tickCramming() {
        super.tickCramming();
    }

    @Override
    public boolean damage(DamageSource source, float amount) {
        if ((source.isMagic() || source.isFromFalling()) && ! source.isUnblockable()) return false;
        if (source.getAttacker() != null && source.getAttacker() instanceof PlayerEntity) {
//            dropInventory();
//            discard();
        }
        return super.damage(source, amount);
    }

    @Override
    protected void initGoals() {
        super.initGoals();
    }

    @Override
    protected void updateGoalControls() {
        super.updateGoalControls();
    }

    @Override
    public void tick() {
        super.tick();
//        if (!world.isClient) {
//            if (age > 5 && isEmpty()) {
//                if (world.getBlockState(getBlockPos().add(0, -1, 0)).isOf(BlockRegistry.Companion.getROBOPORT())) {
//                    var rpbe = ((RoboportBlockEntity) world.getBlockEntity(getBlockPos().add(0, -1, 0)));
//                    if (rpbe != null) {
//                        rpbe.tryEnterRoboport(this, true);
//                    }
//                }
//            }
//            if (world.getBlockState(getBlockPos()).isOf(Blocks.WATER)) setHealth(getHealth() - 0.5f);
//        }

    }

    @Override
    public boolean isCustomNameVisible() {
        return false;
    }

    @Override
    public boolean hasCustomName() {
        return !isDead();
    }

    @Override
    public boolean isOnGround() {
        return true;
    }

    @Override
    protected void dropInventory() {
        ItemScatterer.spawn(world, getBlockPos(), items);
        super.dropInventory();
    }

    @Override
    public void onDeath(DamageSource source) {
        if (LogisticNetwork.robots.contains(this)) LogisticNetwork.removeRobot(this);
        super.onDeath(source);
    }

    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }

    @Override
    public void onRemoved() {

        LogisticNetwork.removeRobot(this);
        super.onRemoved();
    }

    public int getRobotId() {
        return dataTracker.get(ID);
    }

    public BlockPos getTargetPos() {
        return dataTracker.get(TARGET);
    }

    public void setTargetPos(BlockPos target) {
        dataTracker.set(TARGET, target);
    }

    @Override
    public String toString() {
        return "%s Robot, id: %d".formatted(getRobotType(), getRobotId());
    }

    @Override
    public boolean canImmediatelyDespawn(double d) {
        return false;
    }
}
