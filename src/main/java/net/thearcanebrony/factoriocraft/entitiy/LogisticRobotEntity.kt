package net.thearcanebrony.factoriocraft.entitiy

import com.google.common.collect.Lists
import net.minecraft.block.*
import net.minecraft.block.enums.DoubleBlockHalf
import net.minecraft.client.render.entity.EntityRendererFactory
import net.minecraft.entity.EntityDimensions
import net.minecraft.entity.EntityGroup
import net.minecraft.entity.EntityPose
import net.minecraft.entity.EntityType
import net.minecraft.entity.ai.AboveGroundTargeting
import net.minecraft.entity.ai.NoPenaltySolidTargeting
import net.minecraft.entity.ai.NoWaterTargeting
import net.minecraft.entity.ai.control.FlightMoveControl
import net.minecraft.entity.ai.control.LookControl
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.ai.goal.GoalSelector
import net.minecraft.entity.ai.goal.SwimGoal
import net.minecraft.entity.ai.pathing.BirdNavigation
import net.minecraft.entity.ai.pathing.EntityNavigation
import net.minecraft.entity.ai.pathing.Path
import net.minecraft.entity.ai.pathing.PathNodeType
import net.minecraft.entity.attribute.DefaultAttributeContainer
import net.minecraft.entity.attribute.EntityAttributes
import net.minecraft.entity.damage.DamageSource
import net.minecraft.entity.data.DataTracker
import net.minecraft.entity.data.TrackedData
import net.minecraft.entity.data.TrackedDataHandlerRegistry
import net.minecraft.fluid.Fluid
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtHelper
import net.minecraft.particle.ParticleEffect
import net.minecraft.particle.ParticleTypes
import net.minecraft.server.world.ServerWorld
import net.minecraft.sound.SoundEvent
import net.minecraft.sound.SoundEvents
import net.minecraft.state.property.IntProperty
import net.minecraft.tag.BlockTags
import net.minecraft.tag.TagKey
import net.minecraft.util.Identifier
import net.minecraft.util.annotation.Debug
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.Vec3d
import net.minecraft.util.math.Vec3i
import net.minecraft.world.World
import net.minecraft.world.WorldEvents
import net.minecraft.world.WorldView
import net.thearcanebrony.factoriocraft.FactorioCraft
import net.thearcanebrony.factoriocraft.block.entity.RoboportBlockEntity
import net.thearcanebrony.factoriocraft.registries.BlockEntityRegistry
import net.thearcanebrony.factoriocraft.registries.BlockRegistry
import software.bernie.geckolib3.core.IAnimatable
import software.bernie.geckolib3.core.PlayState
import software.bernie.geckolib3.core.builder.AnimationBuilder
import software.bernie.geckolib3.core.controller.AnimationController
import software.bernie.geckolib3.core.event.predicate.AnimationEvent
import software.bernie.geckolib3.core.manager.AnimationData
import software.bernie.geckolib3.core.manager.AnimationFactory
import software.bernie.geckolib3.model.AnimatedGeoModel
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer
import java.util.*
import java.util.function.Predicate
import kotlin.experimental.and
import kotlin.experimental.or

class LogisticRobotEntity(entityType: EntityType<LogisticRobotEntity>, world: World) : BaseRobotEntity(entityType, world), IAnimatable {
	companion object{
		val BEE_FLAGS: TrackedData<Byte>  = DataTracker.registerData(LogisticRobotEntity::class.java, TrackedDataHandlerRegistry.BYTE)
		val ANGER: TrackedData<Int> = DataTracker.registerData(LogisticRobotEntity::class.java, TrackedDataHandlerRegistry.INTEGER)!!

		fun createBeeAttributes(): DefaultAttributeContainer.Builder {
			return createMobAttributes().add(
				EntityAttributes.GENERIC_MAX_HEALTH, 10.0
			).add(
				EntityAttributes.GENERIC_FLYING_SPEED, 0.6
			).add(
				EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.3
			).add(
				EntityAttributes.GENERIC_ATTACK_DAMAGE, 2.0
			).add(
				EntityAttributes.GENERIC_FOLLOW_RANGE, 48.0
			)
		}
	}
	@Suppress("removal")
	private val factory = AnimationFactory(this)
//	val field_30271 = 120.32113f
	val field_28638 = MathHelper.ceil(1.4959966f)
	private val NEAR_TARGET_FLAG = 2
	private val HAS_STUNG_FLAG = 4
	private val HAS_NECTAR_FLAG = 8
//	private val MAX_LIFETIME_AFTER_STINGING = 1200

	/**
	 * A bee will start moving to a flower once this time in ticks has passed from a pollination.
	 */
//	private val FLOWER_NAVIGATION_START_TICKS = 2400

	/**
	 * The duration in ticks when a bee's pollination is considered failed.
	 */
//	private val POLLINATION_FAIL_TICKS = 3600
//	private val field_30287 = 4
//	private val MAX_POLLINATED_CROPS = 10
//	private val NORMAL_DIFFICULTY_STING_POISON_DURATION = 10
//	private val HARD_DIFFICULTY_STING_POISON_DURATION = 18

	/**
	 * The minimum distance that bees lose their port or flower position at.
	 */
	private val TOO_FAR_DISTANCE = 120
//	private val field_30292 = 2

	/**
	 * The minimum distance that bees will immediately return to their port at.
	 */
//	private val MIN_PORT_RETURN_DISTANCE = 50
//	private val field_30294 = 20
	val CROPS_GROWN_SINCE_POLLINATION_KEY = "CropsGrownSincePollination"
	val CANNOT_ENTER_ROBOPORT_TICKS_KEY = "CannotEnterRoboportTicks"
	val TICKS_SINCE_POLLINATION_KEY = "TicksSincePollination"
	val HAS_STUNG_KEY = "HasStung"
	val HAS_NECTAR_KEY = "HasNectar"
	val FLOWER_POS_KEY = "FlowerPos"
	val ROBOPORT_POS_KEY = "RoboportPos"
//	private val ANGER_TIME_RANGE = TimeHelper.betweenSeconds(20, 39)
//	private val targetUuid: UUID? = null
	private var currentPitch = 0f
	private var lastPitch = 0f
	private var ticksSinceSting = 0
	var ticksSincePollination = 0
	private var cannotEnterRoboportTicks = 0
	private var cropsGrownSincePollination = 0
	var ticksLeftToFindRoboport = 0
	var ticksUntilCanPollinate = 0
	var flowerPos: BlockPos? = null
	var roboportPos: BlockPos? = null
	lateinit var pollinateGoal: PollinateGoal
	lateinit var moveToRoboportGoal: MoveToRoboportGoal
	lateinit var moveToFlowerGoal: MoveToFlowerGoal
	private var ticksInsideWater = 0


	init {
		ignoreCameraFrustum = true
		moveControl = FlightMoveControl(this, 20, true)
		lookControl = LogisticRobotLookControl(this)
		setPathfindingPenalty(PathNodeType.DANGER_FIRE, -1.0f)
		setPathfindingPenalty(PathNodeType.WATER, -1.0f)
		setPathfindingPenalty(PathNodeType.WATER_BORDER, 16.0f)
		setPathfindingPenalty(PathNodeType.COCOA, -1.0f)
		setPathfindingPenalty(PathNodeType.FENCE, -1.0f)
//		noClip = true
	}

	private fun <E : IAnimatable?> predicate(event: AnimationEvent<E>): PlayState {
		@Suppress("removal")
		event.controller.setAnimation(AnimationBuilder().addAnimation("animation.logistic_robot_model.idle", true))
		return PlayState.CONTINUE
	}

	override fun registerControllers(data: AnimationData) {
		data.addAnimationController(AnimationController(this, "controller", 0F, ::predicate))
	}

	override fun getFactory(): AnimationFactory {
		return this.factory
	}

	internal class LogisticRobotEntityRenderer(renderManager: EntityRendererFactory.Context) : GeoEntityRenderer<LogisticRobotEntity>(renderManager, LogisticRobotEntityModel()) {
		init {
			shadowRadius = 0.3f
		}
	}

	internal class LogisticRobotEntityModel : AnimatedGeoModel<LogisticRobotEntity>() {
		override fun getModelResource(`object`: LogisticRobotEntity?): Identifier {
			return Identifier(FactorioCraft.ModId, "geo/logistic_robot.geo.json")
		}

		override fun getTextureResource(`object`: LogisticRobotEntity?): Identifier {
			return Identifier(FactorioCraft.ModId, "textures/entity/logistic_robot_base.png")
		}

		override fun getAnimationResource(animatable: LogisticRobotEntity?): Identifier {
			return Identifier(FactorioCraft.ModId, "animations/logistic_robot_model.animation.json")
		}
	}

	override fun getRobotType(): String = "Meow"

	override fun initDataTracker() {
		super.initDataTracker()
		dataTracker.startTracking(BEE_FLAGS, 0.toByte())
		dataTracker.startTracking(ANGER, 0)
	}

	override fun getPathfindingFavor(pos: BlockPos?, world: WorldView): Float = if(world.getBlockState(pos).isAir) 10.0f else 0.0f

	override fun initGoals() {
		goalSelector.add(0, SwimGoal(this))
		goalSelector.add(2, EnterRoboportGoal())
		pollinateGoal = PollinateGoal()
		goalSelector.add(3, pollinateGoal)
		goalSelector.add(4, FindRoboportGoal())
		moveToRoboportGoal = MoveToRoboportGoal()
		goalSelector.add(4, moveToRoboportGoal)
		moveToFlowerGoal = MoveToFlowerGoal()
		goalSelector.add(5, moveToFlowerGoal)
		goalSelector.add(6, GrowCropsGoal())
		goalSelector.add(7, BeeWanderAroundGoal())
	}

	override fun writeCustomDataToNbt(nbt: NbtCompound) {
		super.writeCustomDataToNbt(nbt)
		if(hasRoboport()) {
			nbt.put(ROBOPORT_POS_KEY, NbtHelper.fromBlockPos(roboportPos))
		}
		if(hasFlower()) {
			nbt.put(FLOWER_POS_KEY, NbtHelper.fromBlockPos(flowerPos))
		}
		nbt.putBoolean(HAS_NECTAR_KEY, hasNectar())
		nbt.putBoolean(HAS_STUNG_KEY, hasStung())
		nbt.putInt(TICKS_SINCE_POLLINATION_KEY, ticksSincePollination)
		nbt.putInt(CANNOT_ENTER_ROBOPORT_TICKS_KEY, cannotEnterRoboportTicks)
		nbt.putInt(CROPS_GROWN_SINCE_POLLINATION_KEY, cropsGrownSincePollination)
	}

	override fun readCustomDataFromNbt(nbt: NbtCompound) {
		roboportPos = null
		if(nbt.contains(ROBOPORT_POS_KEY)) {
			roboportPos = NbtHelper.toBlockPos(nbt.getCompound(ROBOPORT_POS_KEY))
		}
		flowerPos = null
		if(nbt.contains(FLOWER_POS_KEY)) {
			flowerPos = NbtHelper.toBlockPos(nbt.getCompound(FLOWER_POS_KEY))
		}
		super.readCustomDataFromNbt(nbt)
		setHasNectar(nbt.getBoolean(HAS_NECTAR_KEY))
		setHasStung(nbt.getBoolean(HAS_STUNG_KEY))
		ticksSincePollination = nbt.getInt(TICKS_SINCE_POLLINATION_KEY)
		cannotEnterRoboportTicks = nbt.getInt(CANNOT_ENTER_ROBOPORT_TICKS_KEY)
		cropsGrownSincePollination = nbt.getInt(CROPS_GROWN_SINCE_POLLINATION_KEY)
	}

	override fun tick() {
		super.tick()
		if(hasNectar() && getCropsGrownSincePollination() < 10 && random.nextFloat() < 0.05f) {
			for(i in 0 until random.nextInt(2) + 1) {
				addParticle(world, this.x - 0.3, this.x + 0.3, this.z - 0.3, this.z + 0.3, getBodyY(0.5), ParticleTypes.FALLING_NECTAR)
			}
		}
		updateBodyPitch()
	}

	private fun addParticle(world: World, lastX: Double, x: Double, lastZ: Double, z: Double, y: Double, @Suppress("SameParameterValue") effect: ParticleEffect) {
		world.addParticle(effect, MathHelper.lerp(world.random.nextDouble(), lastX, x), y, MathHelper.lerp(world.random.nextDouble(), lastZ, z), 0.0, 0.0, 0.0)
	}

	fun startMovingTo(pos: BlockPos?) {
		var vec3d2: Vec3d
		val vec3d = Vec3d.ofBottomCenter(pos)
		var i = 0
		val blockPos = blockPos
		val j = vec3d.y.toInt() - blockPos.y
		if(j > 2) {
			i = 4
		} else if(j < -2) {
			i = -4
		}
		var k = 6
		var l = 8
		val m = blockPos.getManhattanDistance(pos)
		if(m < 15) {
			k = m / 2
			l = m / 2
		}
		NoWaterTargeting.find(this, k, l, i, vec3d, 0.3141592741012573).also { vec3d2 = it?:return@startMovingTo }
		navigation.setRangeMultiplier(0.5f)
		navigation.startMovingTo(vec3d2.x, vec3d2.y, vec3d2.z, 1.0)
	}

	fun hasFlower(): Boolean {
		return flowerPos != null
	}

	@Debug fun getMoveGoalTicks(): Int {
		return moveToRoboportGoal.let { Math.max(it.ticks, moveToFlowerGoal.ticks) }
	}

	private fun failedPollinatingTooLong(): Boolean {
		return ticksSincePollination > 3600
	}

	fun canEnterRoboport(): Boolean {
		if(cannotEnterRoboportTicks > 0 || pollinateGoal.isRunning || hasStung() || target != null) {
			return false
		}
		val bl = failedPollinatingTooLong() || world.isRaining || world.isNight || hasNectar()
		return bl && !isRoboportNearFire()
	}

	fun setCannotEnterRoboportTicks(ticks: Int) {
		cannotEnterRoboportTicks = ticks
	}

	fun getBodyPitch(tickDelta: Float): Float {
		return MathHelper.lerp(tickDelta, lastPitch, currentPitch)
	}

	private fun updateBodyPitch() {
		lastPitch = currentPitch
		currentPitch = if(isNearTarget()) Math.min(1.0f, currentPitch + 0.2f) else Math.max(0.0f, currentPitch - 0.24f)
	}

	override fun mobTick() {
		val bl = hasStung()
		ticksInsideWater = if(this.isInsideWaterOrBubbleColumn) ++ticksInsideWater else 0
		if(ticksInsideWater > 20) {
			damage(DamageSource.DROWN, 1.0f)
		}
		if(bl) {
			++ticksSinceSting
			if(ticksSinceSting % 5 == 0 && random.nextInt(MathHelper.clamp(1200 - ticksSinceSting, 1, 1200)) == 0) {
				damage(DamageSource.GENERIC, this.health)
			}
		}
		if(!hasNectar()) {
			++ticksSincePollination
		}
	}

	fun resetPollinationTicks() {
		ticksSincePollination = 0
	}

	private fun isRoboportNearFire(): Boolean {
		if(roboportPos == null) {
			return false
		}
//		val blockEntity = world.getBlockEntity(roboportPos)
		return false /*blockEntity instanceof RoboportBlockEntity && ((RoboportBlockEntity)blockEntity).isNearFire()*/
	}

	private fun doesRoboportHaveSpace(pos: BlockPos): Boolean {
		val blockEntity = world.getBlockEntity(pos)
		return if(blockEntity is RoboportBlockEntity) {
			!blockEntity.isFullOfRobots
		} else false
	}

	@Debug fun hasRoboport(): Boolean {
		return roboportPos != null
	}

	@Debug fun getGoalSelector(): GoalSelector? {
		return goalSelector
	}

	fun getCropsGrownSincePollination(): Int {
		return cropsGrownSincePollination
	}

	private fun resetCropCounter() {
		cropsGrownSincePollination = 0
	}

	fun addCropCounter() {
		++cropsGrownSincePollination
	}

	override fun tickMovement() {
		super.tickMovement()
		if(!world.isClient) {
			if(cannotEnterRoboportTicks > 0) {
				--cannotEnterRoboportTicks
			}
			if(ticksLeftToFindRoboport > 0) {
				--ticksLeftToFindRoboport
			}
			if(ticksUntilCanPollinate > 0) {
				--ticksUntilCanPollinate
			}
			val bl =  /*this.hasAngerTime() && */!hasStung() && target != null && target!!.squaredDistanceTo(this) < 4.0
			setNearTarget(bl)
			if(age % 20 == 0 && !isPortValid()) {
				roboportPos = null
			}
		}
	}

	fun isPortValid(): Boolean {
		if(!hasRoboport()) {
			return false
		}
		val blockEntity = world.getBlockEntity(roboportPos)
		return blockEntity != null && blockEntity.type === BlockEntityRegistry.ROBOPORT_BLOCK_ENTITY
	}

	fun hasNectar(): Boolean {
		return getBeeFlag(HAS_NECTAR_FLAG)
	}

	fun setHasNectar(hasNectar: Boolean) {
		if(hasNectar) {
			resetPollinationTicks()
		}
		setBeeFlag(HAS_NECTAR_FLAG, hasNectar)
	}

	fun hasStung(): Boolean {
		return getBeeFlag(HAS_STUNG_FLAG)
	}

	private fun setHasStung(hasStung: Boolean) {
		setBeeFlag(HAS_STUNG_FLAG, hasStung)
	}

	private fun isNearTarget(): Boolean {
		return getBeeFlag(NEAR_TARGET_FLAG)
	}

	private fun setNearTarget(nearTarget: Boolean) {
		setBeeFlag(NEAR_TARGET_FLAG, nearTarget)
	}

	fun isTooFar(pos: BlockPos): Boolean {
		return !isWithinDistance(pos, TOO_FAR_DISTANCE)
	}

	private fun setBeeFlag(bit: Int, value: Boolean) {
		if(value) {
			dataTracker.set(BEE_FLAGS, dataTracker.get(BEE_FLAGS) or bit.toByte())
		} else {
			dataTracker.set(BEE_FLAGS, dataTracker.get(BEE_FLAGS) and bit.inv().toByte())
		}
	}

	private fun getBeeFlag(location: Int): Boolean = (dataTracker.get(BEE_FLAGS) and location.toByte()).toInt() != 0

	override fun createNavigation(world: World?): EntityNavigation {
		val birdNavigation: BirdNavigation = object : BirdNavigation(this, world) {
			override fun isValidPosition(pos: BlockPos): Boolean {
				return !this.world.getBlockState(pos.down()).isAir
			}

			override fun tick() {
				if(this@LogisticRobotEntity.pollinateGoal.isRunning) {
					return
				}
				super.tick()
			}
		}
		birdNavigation.setCanPathThroughDoors(false)
		birdNavigation.setCanSwim(false)
		birdNavigation.setCanEnterOpenDoors(true)
		return birdNavigation
	}

	fun isFlowers(pos: BlockPos?): Boolean {
		return world.canSetBlock(pos) && world.getBlockState(pos).isIn(BlockTags.FLOWERS)
	}

	override fun playStepSound(pos: BlockPos?, state: BlockState?) {}

	override fun getAmbientSound(): SoundEvent? {
		return null
	}

	override fun getHurtSound(source: DamageSource?): SoundEvent? {
		return SoundEvents.ENTITY_BEE_HURT
	}

	override fun getDeathSound(): SoundEvent? {
		return SoundEvents.ENTITY_BEE_DEATH
	}

	override fun getSoundVolume(): Float {
		return 0.4f
	}

	override fun getActiveEyeHeight(pose: EntityPose?, dimensions: EntityDimensions): Float {
		return if(this.isBaby) {
			dimensions.height * 0.5f
		} else dimensions.height * 0.5f
	}

	override fun handleFallDamage(fallDistance: Float, damageMultiplier: Float, damageSource: DamageSource?): Boolean {
		return false
	}

	override fun fall(heightDifference: Double, onGround: Boolean, landedState: BlockState?, landedPosition: BlockPos?) {}

	override fun hasWings(): Boolean {
		return age % field_28638 == 0
	}

	fun onHoneyDelivered() {
		setHasNectar(false)
		resetCropCounter()
	}

	override fun damage(source: DamageSource?, amount: Float): Boolean {
		if(isInvulnerableTo(source)) {
			return false
		}
		if(!world.isClient) {
			pollinateGoal.cancel()
		}
		return super.damage(source, amount)
	}

	override fun getGroup(): EntityGroup? {
		return EntityGroup.DEFAULT
	}

	override fun swimUpward(fluid: TagKey<Fluid>?) {
		velocity = velocity.add(0.0, 0.01, 0.0)
	}

	fun isWithinDistance(pos: BlockPos, distance: Int): Boolean {
		return pos.isWithinDistance(blockPos, distance.toDouble())
	}

	inner class LogisticRobotLookControl(robotEntity: LogisticRobotEntity) : LookControl(robotEntity) {

		override fun tick() {
			super.tick()
			lookAt(entity.positionTarget.x + .0, entity.positionTarget.y + .0, entity.positionTarget.z + .0)
		}

		override fun shouldStayHorizontal(): Boolean = false

	}

	inner class PollinateGoal : NotAngryGoal() {
		private val flowerPredicate: Predicate<BlockState>
		private var pollinationTicks = 0
		private var lastPollinationTick = 0
		var isRunning = false
			private set
		private var nextTarget: Vec3d? = null
		private var ticks = 0

		init {
			flowerPredicate = Predicate label@{ state: BlockState ->
				if(state.isIn(BlockTags.FLOWERS)) {
					if(state.isOf(Blocks.SUNFLOWER)) {
						return@label state.get(TallPlantBlock.HALF) == DoubleBlockHalf.UPPER
					}
					return@label true
				}
				false
			}
			controls = EnumSet.of(Control.MOVE)
		}

		override fun canRobotStart(): Boolean {
			if(ticksUntilCanPollinate > 0) {
				return false
			}
			if(hasNectar()) {
				return false
			}
			if(world.isRaining()) {
				return false
			}
			val optional = flower
			if(optional.isPresent) {
				flowerPos = optional.get()
				navigation.startMovingTo(flowerPos!!.x + 0.5, flowerPos!!.y + 0.5, flowerPos!!.z + 0.5, 1.2)
				return true
			}
			ticksUntilCanPollinate = MathHelper.nextInt(random, 20, 60)
			return false
		}

		override fun canRobotContinue(): Boolean {
			if(!isRunning) {
				return false
			}
			if(!hasFlower()) {
				return false
			}
			if(world.isRaining()) {
				return false
			}
			if(completedPollination()) {
				return random.nextFloat() < 0.2f
			}
			if(age % 20 == 0 && !isFlowers(flowerPos)) {
				flowerPos = null
				return false
			}
			return true
		}

		private fun completedPollination(): Boolean {
			return pollinationTicks > 400
		}

		fun cancel() {
			isRunning = false
		}

		override fun start() {
			pollinationTicks = 0
			ticks = 0
			lastPollinationTick = 0
			isRunning = true
			resetPollinationTicks()
		}

		override fun stop() {
			if(completedPollination()) {
				setHasNectar(true)
			}
			isRunning = false
			navigation.stop()
			ticksUntilCanPollinate = 200
		}

		override fun requiresUpdateEveryTick(): Boolean {
			return true
		}

		override fun tick() {
			++ticks
			if(ticks > 600) {
				flowerPos = null
				return
			}
			val vec3d = Vec3d.ofBottomCenter(flowerPos).add(0.0, 0.6, 0.0)
			if(vec3d.distanceTo(getPos()) > 1.0) {
				nextTarget = vec3d
				moveToNextTarget()
				return
			}
			if(nextTarget == null) {
				nextTarget = vec3d
			}
			val bl: Boolean = getPos().distanceTo(nextTarget) <= 0.1
			var bl2 = true
			if(!bl && ticks > 600) {
				flowerPos = null
				return
			}
			if(bl) {
				val bl3: Boolean = random.nextInt(25) == 0
				if(bl3) {
					nextTarget = Vec3d(vec3d.getX() + randomOffset.toDouble(), vec3d.getY(), vec3d.getZ() + randomOffset.toDouble())
					navigation.stop()
				} else {
					bl2 = false
				}
				getLookControl().lookAt(vec3d.getX(), vec3d.getY(), vec3d.getZ())
			}
			if(bl2) {
				moveToNextTarget()
			}
			++pollinationTicks
			if(random.nextFloat() < 0.05f && pollinationTicks > lastPollinationTick + 60) {
				lastPollinationTick = pollinationTicks
				playSound(SoundEvents.ENTITY_BEE_POLLINATE, 1.0f, 1.0f)
			}
		}

		private fun moveToNextTarget() {
			getMoveControl().moveTo(nextTarget!!.getX(), nextTarget!!.getY(), nextTarget!!.getZ(), 0.35)
		}

		private val randomOffset: Float
			get() = (random.nextFloat() * 2.0f - 1.0f) * 0.33333334f
		private val flower: Optional<BlockPos>
			get() = findFlower(flowerPredicate, 5.0)

		private fun findFlower(predicate: Predicate<BlockState>, searchDistance: Double): Optional<BlockPos> {
			val blockPos: BlockPos = getBlockPos()
			val mutable = BlockPos.Mutable()
			var i = 0
			while(i <= searchDistance) {
				var j = 0
				while(j < searchDistance) {
					var k = 0
					while(k <= j) {
						var l = if(k < j && k > -j) j else 0
						while(l <= j) {
							mutable[blockPos, k, i - 1] = l
							if(blockPos.isWithinDistance(mutable, searchDistance) && predicate.test(world.getBlockState(mutable))) {
								return Optional.of(mutable)
							}
							l = if(l > 0) -l else 1 - l
						}
						k = if(k > 0) -k else 1 - k
					}
					++j
				}
				i = if(i > 0) -i else 1 - i
			}
			return Optional.empty()
		}
	}

	inner class EnterRoboportGoal : NotAngryGoal() {

		override fun canRobotStart(): Boolean {
			if(
				hasRoboport() &&
				canEnterRoboport() &&
				roboportPos!!.isWithinDistance(blockPos, 2.0)
			) {
				if((world.getBlockEntity(roboportPos) as? RoboportBlockEntity)?.isFullOfRobots == true) {
					roboportPos = null
				} else {
					return world.getBlockEntity(roboportPos) is RoboportBlockEntity
				}
			}
			return false
		}


		override fun canRobotContinue() = false

		override fun start() {
			(world.getBlockEntity(roboportPos) as? RoboportBlockEntity)?.tryEnterRoboport(this@LogisticRobotEntity, hasNectar())
		}
	}

	inner class FindRoboportGoal : NotAngryGoal() {
		override fun canRobotStart(): Boolean {
			return ticksLeftToFindRoboport == 0 && !hasRoboport() && canEnterRoboport()
		}

		override fun canRobotContinue(): Boolean {
			return false
		}

		override fun start() {
			ticksLeftToFindRoboport = 200
			val list = nearbyFreeRoboports
			if(list.isEmpty()) {
				return
			}
			for(blockPos in list) {
				if(moveToRoboportGoal.isPossiblePort(blockPos)) continue
				roboportPos = blockPos
				return
			}
			moveToRoboportGoal.clearPossibleRoboports()
			roboportPos = list[0]
		}

		private val nearbyFreeRoboports: List<BlockPos>
			get() {
				val blockPos: BlockPos = blockPos
				val pointOfInterestStorage = (world as ServerWorld).pointOfInterestStorage
				//val stream = pointOfInterestStorage.getInCircle({ poiType: RegistryEntry<PointOfInterestType>? -> poiType === PointOfInterestRegistry.ROBOPORT }, blockPos, 100, PointOfInterestStorage.OccupationStatus.HAS_SPACE)
				//return stream.map { obj: PointOfInterest -> obj.pos }.filter { pos: BlockPos -> doesRoboportHaveSpace(pos) }.sorted(Comparator.comparingDouble { blockPos2: BlockPos -> blockPos2.getSquaredDistance(blockPos) }).collect(Collectors.toList())
				return emptyList()
			}
	}

	@Debug
	inner class MoveToRoboportGoal : NotAngryGoal() {
		var ticks: Int
		val possibleRoboports: MutableList<BlockPos>
		private var path: Path? = null
		private var ticksUntilLost = 0

		init {
			ticks = world.random.nextInt(10)
			possibleRoboports = Lists.newArrayList()
			controls = EnumSet.of(Control.MOVE)
		}

		override fun canRobotStart(): Boolean {
			return roboportPos != null &&
			       !hasPositionTarget() &&
			       canEnterRoboport() &&
			       !isCloseEnough(roboportPos!!) &&
			       world.getBlockState(roboportPos).isOf(BlockRegistry.ROBOPORT)
		}

		override fun canRobotContinue(): Boolean {
			return canRobotStart()
		}

		override fun start() {
			ticks = 0
			ticksUntilLost = 0
			super.start()
		}

		override fun stop() {
			ticks = 0
			ticksUntilLost = 0
			navigation.stop()
			navigation.resetRangeMultiplier()
		}

		override fun tick() {
			if(roboportPos == null) {
				return
			}
			++ticks
			if(ticks > getTickCount(600)) {
				makeChosenPortPossiblePort()
				return
			}
			if(navigation.isFollowingPath()) {
				return
			}
			if(isWithinDistance(roboportPos!!, 16)) {
				val bl = startMovingToFar(roboportPos!!)
				if(!bl) {
					makeChosenPortPossiblePort()
				} else if(path != null && navigation.currentPath!!.equalsPath(path)) {
					++ticksUntilLost
					if(ticksUntilLost > 60) {
						setLost()
						ticksUntilLost = 0
					}
				} else {
					path = navigation.getCurrentPath()
				}
				return
			}
			if(isTooFar(roboportPos!!)) {
				setLost()
				return
			}
			startMovingTo(roboportPos)
		}

		private fun startMovingToFar(pos: BlockPos): Boolean {
			navigation.setRangeMultiplier(10.0f)
			navigation.startMovingTo(pos.x.toDouble(), pos.y.toDouble(), pos.z.toDouble(), 1.0)
			return navigation.getCurrentPath() != null && navigation.getCurrentPath()!!.reachesTarget()
		}

		fun isPossiblePort(pos: BlockPos): Boolean {
			return possibleRoboports.contains(pos)
		}

		private fun addPossibleRoboport(pos: BlockPos) {
			possibleRoboports.add(pos)
			while(possibleRoboports.size > 3) {
				possibleRoboports.removeAt(0)
			}
		}

		fun clearPossibleRoboports() {
			possibleRoboports.clear()
		}

		private fun makeChosenPortPossiblePort() {
			roboportPos?.let { addPossibleRoboport(it) }
			setLost()
		}

		private fun setLost() {
			roboportPos = null
			ticksLeftToFindRoboport = 200
		}

		private fun isCloseEnough(pos: BlockPos): Boolean {
			if(isWithinDistance(pos, 2)) {
				return true
			}
			val path: Path? = navigation.getCurrentPath()
			return path != null && path.target == pos && path.reachesTarget() && path.isFinished
		}

	}

	inner class MoveToFlowerGoal : NotAngryGoal() {
		var ticks: Int

		init {
			ticks = world.random.nextInt(10)
			controls = EnumSet.of(Control.MOVE)
		}

		override fun canRobotStart(): Boolean {
			return flowerPos != null && !hasPositionTarget() && shouldMoveToFlower() && isFlowers(flowerPos) && !isWithinDistance(flowerPos!!, 2)
		}

		override fun canRobotContinue(): Boolean {
			return canRobotStart()
		}

		override fun start() {
			ticks = 0
			super.start()
		}

		override fun stop() {
			ticks = 0
			navigation.stop()
			navigation.resetRangeMultiplier()
		}

		override fun tick() {
			if(flowerPos == null) {
				return
			}
			++ticks
			if(ticks > getTickCount(600)) {
				flowerPos = null
				return
			}
			if(navigation.isFollowingPath()) {
				return
			}
			if(isTooFar(flowerPos!!)) {
				flowerPos = null
				return
			}
			startMovingTo(flowerPos)
		}

		private fun shouldMoveToFlower(): Boolean {
			return ticksSincePollination > 2400
		}
	}

	inner class GrowCropsGoal : NotAngryGoal() {
		override fun canRobotStart(): Boolean {
			if(getCropsGrownSincePollination() >= 10) {
				return false
			}
			return if(random.nextFloat() < 0.3f) {
				false
			} else hasNectar() && isPortValid()
		}

		override fun canRobotContinue(): Boolean {
			return canRobotStart()
		}

		override fun tick() {
			if(random.nextInt(getTickCount(30)) != 0) {
				return
			}
			for(i in 1..2) {
				val blockPos: BlockPos = getBlockPos().down(i)
				val blockState: BlockState = world.getBlockState(blockPos)
				val block = blockState.block
				var bl = false
				var intProperty: IntProperty? = null
				if(!blockState.isIn(BlockTags.BEE_GROWABLES)) continue
				if(block is CropBlock) {
					val cropBlock = block
					if(!cropBlock.isMature(blockState)) {
						bl = true
						intProperty = cropBlock.ageProperty
					}
				} else if(block is StemBlock) {
					val cropBlock = blockState.get(StemBlock.AGE)
					if(cropBlock < 7) {
						bl = true
						intProperty = StemBlock.AGE
					}
				} else if(blockState.isOf(Blocks.SWEET_BERRY_BUSH)) {
					val cropBlock = blockState.get(SweetBerryBushBlock.AGE)
					if(cropBlock < 3) {
						bl = true
						intProperty = SweetBerryBushBlock.AGE
					}
				} else if(blockState.isOf(Blocks.CAVE_VINES) || blockState.isOf(Blocks.CAVE_VINES_PLANT)) {
					(blockState.block as Fertilizable).grow(world as ServerWorld, random, blockPos, blockState)
				}
				if(!bl) continue
				world.syncWorldEvent(WorldEvents.PLANT_FERTILIZED, blockPos, 0)
				world.setBlockState(blockPos, blockState.with(intProperty, blockState.get(intProperty) + 1))
				addCropCounter()
			}
		}
	}

	inner class BeeWanderAroundGoal : Goal() {
		init {
			controls = EnumSet.of(Control.MOVE)
		}

		override fun canStart(): Boolean {
			return navigation.isIdle() && random.nextInt(10) == 0
		}

		override fun shouldContinue(): Boolean {
			return navigation.isFollowingPath()
		}

		override fun start() {
			val vec3d = randomLocation
			if(vec3d != null) {
				navigation.startMovingAlong(navigation.findPathTo(BlockPos(vec3d), 1), 1.0)
			}
		}

		private val randomLocation: Vec3d?
			get() {
				val vec3d2: Vec3d
				vec3d2 = if(isPortValid() && !roboportPos?.let { isWithinDistance(it, 22) }!!) {
					val vec3d = Vec3d.ofCenter(roboportPos)
					vec3d.subtract(getPos()).normalize()
				} else {
					getRotationVec(0.0f)
				}
				val vec3d3 = AboveGroundTargeting.find(this@LogisticRobotEntity, 8, 7, vec3d2.x, vec3d2.z, 1.5707964f, 3, 1)
				return vec3d3
				       ?: NoPenaltySolidTargeting.find(this@LogisticRobotEntity, 8, 4, -2, vec3d2.x, vec3d2.z, 1.5707963705062866)
			}

	}

	inner abstract class NotAngryGoal : Goal() {
		abstract fun canRobotStart(): Boolean
		abstract fun canRobotContinue(): Boolean
		override fun canStart(): Boolean {
			return canRobotStart()
		}

		override fun shouldContinue(): Boolean {
			return canRobotContinue()
		}
	}
}

