package net.thearcanebrony.factoriocraft.entitiy.renderers;

import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.util.Identifier;
import net.thearcanebrony.factoriocraft.client.FactorioCraftClient;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;
import net.thearcanebrony.factoriocraft.entitiy.models.LogisticsRobotEntityModel;

public class LogisticsRobotEntityRenderer extends MobEntityRenderer<BaseRobotEntity, LogisticsRobotEntityModel> {
    public LogisticsRobotEntityRenderer(EntityRendererFactory.Context context) {
        super(context, new LogisticsRobotEntityModel(context.getPart(FactorioCraftClient.getMODEL_CUBE_LAYER())), 0.5f);
    }

    @Override
    public Identifier getTexture(BaseRobotEntity entity) {
        return new Identifier("factoriocraft", "textures/entity/logistic_robot/logistic_robot.png");
    }
}
