package net.thearcanebrony.factoriocraft.entitiy;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ai.goal.MoveToTargetPosGoal;
import net.minecraft.entity.ai.goal.SwimGoal;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.PositionUtils;
import net.thearcanebrony.factoriocraft.goals.robot.GoToRoboportGoal;
import net.thearcanebrony.factoriocraft.registries.BlockRegistry;

import java.util.Random;

public class LogisticsRobotEntity extends BaseRobotEntity {
    private static final Random rnd = new Random();
    private long ticks = 0;
    private int randomTickOffset = 19;

    public LogisticsRobotEntity(EntityType<? extends BaseRobotEntity> entityType, World world) {
        super(entityType, world);
//        id = next_id;
//        if (!world.isClient) next_id++;
        items.set(0, new ItemStack(Items.DIAMOND, 1));
    }

    @Override
    public String getRobotType(){
        return "Logistic";

    }

    @Override
    protected void tickCramming() {
//        super.tickCramming();

    }
    private MoveToTargetPosGoal mttpg;
    private GoToRoboportGoal gtrg;
    @Override
    protected void initGoals() {
        super.initGoals();
        goalSelector.add(1, new SwimGoal(this) {
            @Override
            public boolean shouldContinue() {
                return world.getBlockState(getBlockPos()).isOf(Blocks.WATER);
            }
        });
        goalSelector.add(2, gtrg = new GoToRoboportGoal(this, 100));


        // goalSelector.add(20, new WanderAroundGoal(this, 0.5, 25));
    }

    @Override
    protected void updateGoalControls() {
        super.updateGoalControls();
        if ((getRobotId() + ticks) % 20 == randomTickOffset) {

            if (getTargetPos() == null || !world.getBlockState(getTargetPos()).isOf(BlockRegistry.ROBOPORT)) {
                long ctime = System.nanoTime();
                setTargetPos(PositionUtils.searchRoboport(this, 100));
                gtrg.target = getTargetPos();
                if(getTargetPos() == null){
//                    System.out.printf("Robot %s did not find any target in %.4f\n", id, (System.nanoTime() - ctime) / 1000000d);
                } else {
                    gtrg.updatePathFinder();
//                    System.out.printf("Robot %s found roboport %s in %.4f ms\n", id, target, (System.nanoTime() - ctime) / 1000000d);
                }
            }
        }

        if (!world.isClient) ticks++;
    }

    @Override
    public void tick() {
        super.tick();
        if (!world.isClient) {
            if((getRobotId() + world.getTime()) % 20 == 0) {
            }
            gtrg.target = getTargetPos();
//            ticks++;
        } else {

            if(getTargetPos() != null) {
                setCustomName(Text.literal(String.format("Robot %s: %s/%s/%s (HP: %.2f)", dataTracker.get(ID), getTargetPos().getX(), getTargetPos().getY(), getTargetPos().getZ(), getHealth())));
            } else {
                setCustomName(Text.literal(String.format("Robot %s: No Target (HP: %.2f)", dataTracker.get(ID), getHealth())));
            }
        }

    }


    private final DefaultedList<ItemStack> items = DefaultedList.ofSize(2, ItemStack.EMPTY);

    @Override
    public DefaultedList<ItemStack> getStacks() {
        return items;
    }

    public GoToRoboportGoal getGtrg () {
        return gtrg;
    }

    public void setGtrg (GoToRoboportGoal gtrg) {
        this.gtrg = gtrg;
    }

    //    @Nullable
//    @Override
//    public Text getCustomName() {
////        return Text.literal(String.format("Robot %s: %s/%s/%s", getSavedEntityId(), target.getX(), target.getY(), target.getZ()));
//        return super.getCustomName();
//    }
}
