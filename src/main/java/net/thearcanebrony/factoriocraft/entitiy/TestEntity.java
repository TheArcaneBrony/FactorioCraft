package net.thearcanebrony.factoriocraft.entitiy;

import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;
import net.thearcanebrony.factoriocraft.FactorioCraft;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;

public class TestEntity extends PathAwareEntity implements IAnimatable {
    private final AnimationFactory factory = new AnimationFactory(this);

    public TestEntity(EntityType<? extends PathAwareEntity> type, World worldIn) {
        super(type, worldIn);
        this.ignoreCameraFrustum = true;
    }

    private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
        event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.logistic_robot_model.idle", true));
        return PlayState.CONTINUE;
    }

    @Override
    public void registerControllers(AnimationData data) {
        data.addAnimationController(new AnimationController<>(this, "controller", 0, this::predicate));
    }

    @Override
    public AnimationFactory getFactory() {
        return this.factory;
    }

    public static class TestEntityRenderer extends GeoEntityRenderer<TestEntity>{
        public TestEntityRenderer(EntityRendererFactory.Context renderManager){
            super(renderManager, new TestEntityModel());
            this.shadowRadius = 0.3f;
        }
    }

    public static class TestEntityModel extends AnimatedGeoModel<TestEntity> {
        @Override
        public Identifier getModelResource(TestEntity object) {
            return new Identifier(FactorioCraft.ModId, "geo/logistic_robot.geo.json");
        }

        @Override
        public Identifier getTextureResource(TestEntity object) {
            return new Identifier(FactorioCraft.ModId, "textures/entity/logistic_robot_base.png");
        }

        @Override
        public Identifier getAnimationResource(TestEntity animatable) {
            return  new Identifier(FactorioCraft.ModId, "animations/logistic_robot_model.animation.json");
        }
    }
}
