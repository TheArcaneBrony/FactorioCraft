package net.thearcanebrony.factoriocraft.entitiy.models;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.*;
import net.minecraft.client.render.entity.model.EntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;

public class LogisticsRobotEntityModel extends EntityModel<BaseRobotEntity> {
    private final ModelPart base;

    public LogisticsRobotEntityModel(ModelPart modelPart) {
        this.base = modelPart.getChild("center");
    }

    public static TexturedModelData getTexturedModelData() {
        ModelData modelData = new ModelData();
        ModelPartData modelPartData = modelData.getRoot();
        var r = ModelPartBuilder.create()
                .uv(40, 0)
                .cuboid(-1.5F, 11.0F, -1.5F, 3.0F, 11.0F, 3.0F);
        modelPartData.addChild(
                "center",
                r,
                ModelTransform.pivot(0F, 0F, 0F)
        );
        return TexturedModelData.of(modelData, 64, 64);
    }

    @Override
    public void setAngles(BaseRobotEntity entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
    }

    @Override
    public void render(MatrixStack matrices, VertexConsumer vertices, int light, int overlay, float red, float green, float blue, float alpha) {
        ImmutableList.of(this.base).forEach((modelRenderer) -> modelRenderer.render(matrices, vertices, light, overlay, red, green, blue, alpha));
    }
}
