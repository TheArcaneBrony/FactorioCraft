package net.thearcanebrony.factoriocraft.state;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.Identifier;
import net.thearcanebrony.factoriocraft.registries.Technologies;
import net.thearcanebrony.factoriocraft.research.Technology;

import java.util.HashMap;
import java.util.Map;

public class ResearchState {
    public Map<Technology, Integer> states = new HashMap<>();
    public Technology currentResearch = null;

    public ResearchState() {
        states.put(Technologies.getROOT(), Technologies.getROOT().getResearchCount());
    }

    public void checkUnlocked() {

    }

    public void writeNbt(NbtCompound nbt) {
        for (var key : states.keySet()) {
            nbt.putInt(key.getIdentifier().toString(), states.get(key));
        }
    }

    public void readNbt(NbtCompound nbt) {
        for (var key : nbt.getKeys()) {
            Identifier id;
            if ((id = Identifier.tryParse(key)) != null) {
                states.put(Technologies.getINSTANCE().get(id), nbt.getInt(key));
            }
        }
    }

    public int unlock(Technology technology) {
        if (states.getOrDefault(technology, 0) == technology.getResearchTime() * technology.getResearchCount())
            return 0;
        var unlocked = 1;
        states.put(technology, technology.getResearchTime() * technology.getResearchCount());
        for (var parent : technology.getParents()) {
            unlocked += unlock(parent);
        }
        return unlocked;
    }
}