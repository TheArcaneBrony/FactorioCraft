package net.thearcanebrony.factoriocraft.state;

import com.google.gson.Gson;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.world.PersistentState;

public class FactorioCraftState extends PersistentState {
    private static final Gson GSON = new Gson();

    public final ResearchState researchState = new ResearchState();

    @Override
    public NbtCompound writeNbt(NbtCompound nbt) {
        var researchStateNbt = new NbtCompound();
        researchState.writeNbt(researchStateNbt);
        nbt.put("research", researchStateNbt);
        return nbt;
    }
    public static FactorioCraftState readNbt(NbtCompound nbt) {
        var state =  new FactorioCraftState();
        state.researchState.readNbt(nbt.getCompound("research"));
        return state;
    }

    public void tick() {
        researchState.checkUnlocked();
    }
}
