package net.thearcanebrony.factoriocraft.mixin;

import com.mojang.datafixers.DataFixer;
import net.minecraft.resource.pack.ResourcePackManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.Services;
import net.minecraft.server.WorldGenerationProgressListenerFactory;
import net.minecraft.server.WorldStem;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.level.storage.LevelStorage;
import net.thearcanebrony.factoriocraft.state.FactorioCraftState;
import net.thearcanebrony.factoriocraft.staticdata.FactorioStateHolder;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.net.Proxy;

@Mixin(MinecraftServer.class)
public abstract class ServerMixin implements FactorioStateHolder {

    @Shadow @Final protected LevelStorage.Session session;

    @Shadow public abstract ServerWorld getOverworld();

    @Shadow public abstract void startDebug();

    private FactorioCraftState state;

    @Inject(
            method = "<init>",
            at = @At("RETURN")
    ) void initState(
            Thread serverThread,
            LevelStorage.Session session,
            ResourcePackManager dataPackManager,
            WorldStem worldStem,
            Proxy proxy,
            DataFixer dataFixer,
            Services services,
            WorldGenerationProgressListenerFactory worldGenerationProgressListenerFactory,
            CallbackInfo ci
    ) {
    }

    @Override
    public FactorioCraftState getFactorioState() {
        return state;
    }

    @Override
    public void saveState() {
        if(state != null) {
            state.markDirty();
            getOverworld().getPersistentStateManager().set("factorio-data", state);
            getOverworld().getPersistentStateManager().save();
        }
    }

    @Override
    public void loadState() {
        state = getOverworld().getPersistentStateManager().get(FactorioCraftState::readNbt, "factorio-data");
        if(state == null){
            state = new FactorioCraftState();
        }
    }
}
