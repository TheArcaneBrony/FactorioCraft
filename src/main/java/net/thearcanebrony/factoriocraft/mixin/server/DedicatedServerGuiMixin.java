package net.thearcanebrony.factoriocraft.mixin.server;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.server.dedicated.MinecraftDedicatedServer;
import net.minecraft.server.dedicated.gui.DedicatedServerGui;
import net.thearcanebrony.factoriocraft.client.gui.swing.FactorioCraftDebugPanel;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import javax.swing.*;
import java.awt.*;

import static net.thearcanebrony.factoriocraft.FactorioCraft.debugPanel;
import static org.spongepowered.asm.mixin.injection.At.Shift.BEFORE;

@Environment(EnvType.SERVER)
@Mixin(DedicatedServerGui.class)
public class DedicatedServerGuiMixin extends JComponent {
    JTabbedPane tabsPane;
    JPanel mainPain;

    @Inject(
            method = "<init>",
            at = @At(
                    value = "INVOKE",
                    shift = BEFORE,
                    target = "Lnet/minecraft/server/dedicated/gui/DedicatedServerGui;add(Ljava/awt/Component;Ljava/lang/Object;)V",
                    ordinal = 0
            )
    )
    private void createMainTab(MinecraftDedicatedServer server, CallbackInfo info) {
        tabsPane = new JTabbedPane();
        mainPain = new JPanel(new BorderLayout());
//        mainPain.setLayout();
        tabsPane.add("Minecraft", mainPain);
        debugPanel[0] = new FactorioCraftDebugPanel();
        tabsPane.add("FactorioCraft", debugPanel[0]);
        add(tabsPane, "Center");
    }

    @Redirect(
            method = "<init>",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/server/dedicated/gui/DedicatedServerGui;add(Ljava/awt/Component;Ljava/lang/Object;)V"
            )
    )
    private void addComponentsToMain(DedicatedServerGui instance, Component component, Object constraints) {
        mainPain.add(component, constraints);
    }
}
