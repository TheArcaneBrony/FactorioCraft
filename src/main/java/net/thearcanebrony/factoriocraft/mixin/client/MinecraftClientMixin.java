package net.thearcanebrony.factoriocraft.mixin.client;

import com.mojang.blaze3d.glfw.Window;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.integrated.IntegratedServer;
import net.thearcanebrony.factoriocraft.staticdata.LogisticNetwork;
import org.jetbrains.annotations.Nullable;
import org.quiltmc.loader.api.minecraft.ClientOnly;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.text.DecimalFormat;

@ClientOnly
@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    private static DecimalFormat df = new DecimalFormat("#.##");
    @Shadow public String fpsDebugString;
    @Shadow @Final private String gameVersion;
    @Shadow @Nullable private IntegratedServer server;
    @Shadow @Final private Window window;

    @Inject(method = "tick", at = @At(value = "RETURN"))
    private void updateTitle(CallbackInfo ci) {
        float tickTime = server == null ? 0 : server.getTickTime();
        var perf = String.format("%s FPS, %s TPS (%s MSPT)", fpsDebugString.split(" ")[0], df.format(Math.min(1000 / tickTime, 20)), df.format(tickTime));
        var count = String.format("%s robots, %s roboports, %s chests", LogisticNetwork.robots.size(), LogisticNetwork.roboports.size(),
                (
                    LogisticNetwork.activeProviderChests.size() +
                    LogisticNetwork.passiveProviderChests.size() +
                    LogisticNetwork.requesterChests.size() +
                    LogisticNetwork.storageChests.size()
                ));

        window.setTitle(String.format("FactorioCraft (MC %s) - %s - %s", gameVersion, perf, count));
    }
}
