package net.thearcanebrony.factoriocraft.goals.robot;

import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.pathing.Path;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.thearcanebrony.factoriocraft.FactorioCraft;
import net.thearcanebrony.factoriocraft.entitiy.BaseRobotEntity;
import net.thearcanebrony.factoriocraft.entitiy.pathfinding.RobotPathFinder;

import java.util.Random;

public class GoToRoboportGoal extends Goal {
    public MobEntity entity;
    public int radius;
    public BlockPos target;
    private static final Random rnd = new Random();
    static final double minSpeed = -.2;
    static final double maxSpeed = .2;
    private RobotPathFinder pathFinder;
    Path path;

    public GoToRoboportGoal(BaseRobotEntity entity, int radius) {
        this.entity = entity;
        pathFinder = new RobotPathFinder(entity);
        this.radius = radius;
    }

    @Override
    public boolean canStart() {
        return true;
    }

    @Override
    public boolean requiresUpdateEveryTick() {
        return true;
    }

    public void updatePathFinder(){
        if(target!= null)
        pathFinder.pathToTarget(entity.getBlockPos(), target.add(0, 1, 0));
//        currentTargetNodeIndex = 1;
    }

    @Override
    public void tick() {
        super.tick();
        if(entity.getServer().getTickTime()>20 && rnd.nextInt(101)>50 ) return;
//        if(entity.getServer().getTickTime()>50 && rnd.nextInt(101)>2 ) return;
        if(entity.getServer().getTickTime()>300 && rnd.nextInt(101)>10) return;
        if(entity.getServer().getTickTime() < 200 && entity.getServer().getTickTime()>rnd.nextInt(201)) return;
//        if(target != null && entity.getBlockPos().isWithinDistance(target,0.5)) return;
//        var nav = entity.getNavigation();
        if(target != null) {
//            if (((entity.world.getTime() + ((BaseRobotEntity)entity).id) % 200) == 0) {
//            if(pathFinder.nodes.size() > 1 && pathFinder.nodes.indexOf()){
//
//            }
            if (pathFinder.nodes.size() > 1) {
                if(entity.world.getTime() % 20 == 0){
                    pathFinder.nodes.remove(0);
                } else while (
                        pathFinder.nodes.size() > 1 && (
//                    pathFinder.nodes.get(1).pos.getSquaredDistance(entity.getPos()) < pathFinder.nodes.get(0).pos.getSquaredDistance(entity.getBlockPos()) ||
                                pathFinder.nodes.get(0).pos.equals(new BlockPos(entity.getPos().subtract(Vec3d.of(pathFinder.nodes.get(1).pos.subtract(pathFinder.nodes.get(0).pos)).multiply(.7)))) ||
                                        pathFinder.nodes.get(1).pos.equals(pathFinder.nodes.get(0).pos)
                        )
                )
                    pathFinder.nodes.remove(0);
            } else {
                updatePathFinder();
            }
            if(pathFinder.nodes.size() > 1) {
                if(pathFinder.nodes.get(1).pos.getSquaredDistance(entity.getBlockPos()) > 2.5){
                    long stime = System.nanoTime();
                    updatePathFinder();
                    if (((entity.world.getTime() + ((BaseRobotEntity)entity).getRobotId()) % 8000) == 0) {
                        FactorioCraft.LOGGER.info("found path from {} to {} in {}", entity.getBlockPos(), target, (System.nanoTime() - stime) / 1000000d);
                    }
                }
                pathFinder.tick();
/*                if(currentTargetNodeIndex < pathFinder.nodes.size() - 1 && pathFinder.nodes.get(currentTargetNodeIndex).pos.equals(entity.getBlockPos())){
                    pathFinder.nodes.remove(0);
                } else */if(pathFinder.nodes.size() < 1){
                    updatePathFinder();
                    if(pathFinder.nodes.size() < 1) return;
                }
                BlockPos previousNode = pathFinder.nodes.get(0).pos;
                BlockPos currentNode  = pathFinder.nodes.size() > 1? pathFinder.nodes.get(1).pos : previousNode;
                Vec3d    a            = Vec3d.of(currentNode.subtract(previousNode)).multiply(0.3);
                Vec3d    c            = Vec3d.of(currentNode).subtract(entity.getPos()).add(.5, .06, .5).multiply(.5);
                Vec3d    b            = a.multiply(1, 0, 1).normalize().add(c);
                entity.addVelocity(b.x, b.y, b.z);
                var v = entity.getVelocity();
                v = v.normalize().multiply(MathHelper.clamp(v.length(), minSpeed, maxSpeed));
                entity.setVelocity(v.x, v.y, v.z);
//                nav.startMovingTo(rpf.nodes.get(1).pos.getX(), rpf.nodes.get(1).pos.getY(),rpf.nodes.get(1).pos.getZ(), 0.4);
            } /*else {
                if (((entity.world.getTime() + ((BaseRobotEntity)entity).getRobotId()) % 1) == 0) {
                    updatePathFinder();
                }
            }*/
        }
    }

	public RobotPathFinder getPathFinder () {
		return pathFinder;
	}

	public void setPathFinder (RobotPathFinder pathFinder) {
		this.pathFinder = pathFinder;
	}
}
